<?php  
##############################################################################
##############################################################################
###________________________________________________________________________###
###                                                                        ###
###             vionlink obs 3.4 by vision impress webdesign               ###
###             written 2009/15  by vision impress webdesign               ###
###________________________________________________________________________###
###                                                                        ###
##############################################################################
##############################################################################
         

    error_reporting(0);
    session_start();      
         
##############################################################################
##############################################################################

    define('IMSCRIPT', '1');
    include("administration/inclx/db_vbdg.php");
    include("administration/inclx/funcx.php");
    include("administration/inclx/config.php");

##############################################################################
##############################################################################


    include("inclx/showlist.funcx.php");
    include("inclx/showlist.inc.php");
    include("inclx/openorclosed.php");
    include("inclx/detectmobilebrowser.php");

    include("inclw/oeffnungszeiten.php");
    include("inclw/zusatzstoffe.php");
    include("inclw/produktnavigation.php");


##############################################################################
##############################################################################

    $zliste='';
    $js_4_mess='';
    $open='';
    $summe ='';
    $bnlid='';
    $pp_name='';


##############################################################################
##############################################################################


    if(isset($_POST['submit'])){

        @include("inclx/bestellung_absenden.inc.php");

    }

##############################################################################
##############################################################################


    if($_SESSION['bestellungok']==0){
        $fehler=1;
        $err_mess='der Mindestbestellwert ist nicht erreicht!';
    }

##############################################################################
##############################################################################


    // Template Ordner

    switch($mobile){
        case "0": $template_ordner='templates'; break;
        case "1": $template_ordner='templates_mobile'; break;
    }


##############################################################################
##############################################################################



    // Top Navigation
    $tn_tpl = @file_get_contents("$template_ordner/scme/top_navigation.html");
    $top_navi = preg_replace("=\[PATH\]=", $PATH, $tn_tpl);

    if(isset($_SESSION['kunden_ID'])){
    $top_navi = preg_replace("=\[KUNDENSEITE\]=", 'kundenmenu.php', $top_navi);
    $top_navi = preg_replace("=\[KUNDENSYSTEM\]=", 'Kundenmenu', $top_navi);
    }else{
    $top_navi = preg_replace("=\[KUNDENSEITE\]=", 'kundenlogin.php', $top_navi);
    $top_navi = preg_replace("=\[KUNDENSYSTEM\]=", 'Kunden Login', $top_navi);
    }

    // Footer - Template
    $footer_tpl = @file_get_contents("$template_ordner/scme/footer.html");
    $footer = preg_replace("=\[PATH\]=", $PATH, $footer_tpl);
    $footer = preg_replace("=\[COPYRIGHT\]=", $COPY, $footer);



##############################################################################
##############################################################################



    if(isset($fehler)){
        $fehler_tt='<tr><td align="center"><br><br><div class="fehlermeldung">'.$err_mess.'</div></td></tr>';
    }



##########################################################################################
##########################################################################################


// wenn eine Z-Methode angeschaltet
if(
    $PAYMENT['bar'][0]          == 1 ||
    $PAYMENT['ec'][0]           == 1 ||
    $PAYMENT['paypal'][0]       == 1 ||
    $PAYMENT['sofort'][0]       == 1 ||
    $PAYMENT['masterpayment'][0]== 1
){

    $BEZ ='
    <tr><td colspan="2">&nbsp;</td></tr>
    <tr><td align="right"><b>wie m&ouml;chten Sie zahlen?</b></td><td>';


    $c1 = ($bezahlung=='1')? 'checked' : ''; // Bar
    $c2 = ($bezahlung=='2')? 'checked' : ''; // EC
    $c3 = ($bezahlung=='3')? 'checked' : ''; // PayPal
    $c4 = ($bezahlung=='4')? 'checked' : ''; // SOFORT
    $c5 = ($bezahlung=='5')? 'checked' : ''; // Masterpayment


    if( $PAYMENT['bar'][0]      ==1){$BEZ .='<input type="radio" name="bezahlung" value="1" '.$c1.'>'.$PAYMENT['bar'][1].'<br>';}
    if( $PAYMENT['ec'][0]       ==1){$BEZ .='<input type="radio" name="bezahlung" value="2" '.$c2.'>'.$PAYMENT['ec'][1].'<br>';}
    if( $PAYMENT['paypal'][0]   ==1){$BEZ .='<input type="radio" name="bezahlung" value="3" '.$c3.'>'.$PAYMENT['paypal'][1].'<br>';}
    if( $PAYMENT['sofort'][0]   ==1){$BEZ .='<input type="radio" name="bezahlung" value="4" '.$c4.'>'.$PAYMENT['sofort'][1].'<br>';}
    if( $PAYMENT['masterpayment'][0]   ==1){$BEZ .='<input type="radio" name="bezahlung" value="5" '.$c5.'>'.$PAYMENT['masterpayment'][1].'<br>';}


    $BEZ .='</td></tr><tr><td colspan="2">&nbsp;</td></tr>';

}else{

    $BEZ ='<input type="hidden" name="bezahlung" value="bar">';

}


##########################################################################################
##########################################################################################




if($SELBSTABHOLER==1){

    $d1 = ($selbstabholung == 'on')? 'checked' : '';

    $ckbox_selbstabholer ='    <tr><td colspan="2">&nbsp;</td></tr>
     <tr><td align="right"><b>m&ouml;chten Sie selbstabholen?</b></td><td>';

    $ckbox_selbstabholer .='<input type="checkbox" name="selbstabholung"  '.$d1.'> ja, ich werde die Bestellung selbst abholen <br>';

    $ckbox_selbstabholer .='</td></tr><tr><td colspan="2">&nbsp;</td></tr>';

} else{

    $ckbox_selbstabholer ='';

}




##########################################################################################
##########################################################################################


    $KID=$_SESSION['kunden_ID'];

    $kq=@mysql_query(" SELECT * FROM vob_kunden WHERE id='$KID' ");
    $kr=@mysql_fetch_object($kq);



    // Standard-Lieferanschrift oder im Formular ausgewaehlte Anschrift (-> lg_change.src.php)
    $la_id=$_SESSION['lieferanschrift_ID'];

  
    $LIEFERADRESSEN.='<tr><td align="right">andere Lieferadresse? </td><td>
    <form action="inclx/lg_change.src.php" method="post"><select name="la_id">';

  
    $kq=@mysql_query(" SELECT * FROM vob_lieferanschriften WHERE to_kundenid ='$KID' ");
    $num_kq=@mysql_num_rows($kq);
    while($ks = @mysql_fetch_object($kq)){


          if($ks->id==$la_id){

            $actbest_strasse=$ks->strasse;
            $actbest_nummer=$ks->nummer;
            $actbest_plz=$ks->plz;
            $actbest_ort=$ks->ort;
            $actbest_telefon=$ks->vorwahl.' '.$ks->rufnummer;
            $actbest_firma=$ks->firma;
            $actbest_abteilung=$ks->abteilung;
            $actbest_besonderheit=$ks->besonderheit;

          }else{

            $LIEFERADRESSEN.='<option value="'.$ks->id.'">'.$ks->strasse.' '.$ks->nummer.' '.$ks->plz.' '.$ks->ort.'</option>';

          }

     }


    $LIEFERADRESSEN.='<select>&nbsp;&nbsp;<input type="submit" value="ok" /></form></td></tr>';
    if($num_kq==1){$LIEFERADRESSEN='';}


##########################################################################################
##########################################################################################


     if($closed==1){   // geschlossen, vorbestellung mgl.


        $OPEN_DISPLAY='';
        $form_closed=1;

    }else{
        // kein Vorbestellungskasten
        $form_closed=0;
        $OPEN_DISPLAY=' style="display:none;" ';

    }



// closed 0  IST  open = 1
// closed 1 = Vorbestellung mgl.
// closed 2 = Vorbestellung ausgeschaltet
// closed 3 = Ruhetag (keine Vorbestellung)
// closed 4 = Wartung $WARTUNG


    if($show_newsletter==0){$NEWSLETTER_DISPLAY=' style="display:none;" ';}else{$NEWSLETTER_DISPLAY='';}
    if($show_gutschein==0) {$GUTSCHEIN_DISPLAY=' style="display:none;" ';}else{$GUTSCHEIN_DISPLAY='';}


##########################################################################################
##########################################################################################



    $template = '';
    $inhalt   = '';

    $inhalt = @join('', file("$template_ordner/bestellen_kunde.html"));

    $inhalt = preg_replace("=\[ANREDE\]=", $kr->anrede, $inhalt);
    $inhalt = preg_replace("=\[VORNAME\]=", $kr->vorname, $inhalt);
    $inhalt = preg_replace("=\[NAME\]=", $kr->name, $inhalt);

    $inhalt = preg_replace("=\[STRASSE\]=", $actbest_strasse, $inhalt);
    $inhalt = preg_replace("=\[NUMMER\]=", $actbest_nummer, $inhalt);
    $inhalt = preg_replace("=\[PLZ\]=", $actbest_plz, $inhalt);
    $inhalt = preg_replace("=\[ORT\]=", $actbest_ort, $inhalt);
    $inhalt = preg_replace("=\[TELEFON\]=", $actbest_telefon, $inhalt);
    $inhalt = preg_replace("=\[FIRMA\]=", $actbest_firma, $inhalt);
    $inhalt = preg_replace("=\[ABTEILUNG\]=", $actbest_abteilung, $inhalt);

    if($actbest_besonderheit!=""){
    $inhalt = preg_replace("=\[BESONDERHEITEN\]=", $actbest_besonderheit, $inhalt);
    }else{
    $inhalt = preg_replace("=\[BESONDERHEITEN\]=", '', $inhalt);
    }

    $inhalt = preg_replace("=\[LIEFERADRESSEN\]=", $LIEFERADRESSEN, $inhalt);


    $inhalt = preg_replace("=\[FORM_OPEN\]=", $form_closed, $inhalt);

    $inhalt = preg_replace("=\[FORM_GUTSCHEIN\]=", '', $inhalt);
    $inhalt = preg_replace("=\[BEZAHLUNG\]=", $BEZ, $inhalt);
    $inhalt = preg_replace("=\[SELBSTABHOLUNG\]=", $ckbox_selbstabholer, $inhalt);

    $inhalt = preg_replace("=\[OPEN_DISPLAY\]=", $OPEN_DISPLAY, $inhalt);
    $inhalt = preg_replace("=\[NEWSLETTER_DISPLAY\]=", $NEWSLETTER_DISPLAY, $inhalt);
    $inhalt = preg_replace("=\[GUTSCHEIN_DISPLAY\]=", $GUTSCHEIN_DISPLAY, $inhalt);

    if($mobile==0){
        $inhalt = preg_replace("=\[LOGO_IMG_SRC\]=", 'img/'.$LOGO, $inhalt);
    }else{
        $inhalt = preg_replace("=\[LOGO_IMG_SRC\]=", 'img/'.$MOBILLOGO, $inhalt);
    }
    $inhalt = preg_replace("=\[HEAD_LOGO_IMG_SRC\]=", 'img/'.$HEADLOGO, $inhalt);  

        $inhalt = preg_replace("=\[BACK_LOGO_IMG_SRC\]=", 'img/'.$BACKLOGO, $inhalt);

    $inhalt = preg_replace("=\[MAINCONTENT\]=", $maincontent, $inhalt);
    $inhalt = preg_replace("=\[FOOTER\]=", $footer, $inhalt);
    $inhalt = preg_replace("=\[NAVIGATION\]=", $create_navi, $inhalt);
    $inhalt = preg_replace("=\[TOPNAVIGATION\]=", $top_navi, $inhalt);
    $inhalt = preg_replace("=\[BESTELLSCHEIN\]=", $bestellschein, $inhalt);
    $inhalt = preg_replace("=\[OEFFNUNGSZEITEN\]=", $oeffzeiten, $inhalt);
    $inhalt = preg_replace("=\[FEHLER\]=", $fehler_tt, $inhalt);
    $inhalt = preg_replace("=\[SCHRIFTSATZ\]=", $SCHRIFTSATZ, $inhalt);


    header("Content-Type: text/html; charset=$SCHRIFTSATZ");
    echo $inhalt;
    exit();

##########################################################################################
##########################################################################################
?>