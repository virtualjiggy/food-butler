<?php 
##############################################################################
##############################################################################


$e[0]='Sie m&uuml;ssen die Nutzungsbedingungen akzeptieren';
$e[1]='Sie m&uuml;ssen eine Anrede ausw&auml;hlen';
$e[2]='Bitte geben Sie Ihren Vornamen ein';
$e[3]='Bitte geben Sie Ihren Nachnamen ein';
$e[4]='Bitte geben Sie Ihre Emailadresse ein';
$e[5]='Bitte geben Sie Ihre Vorwahl ein';
$e[6]='Bitte geben Sie Ihre Rufnummer ein';
$e[7]='Bitte geben Sie Ihre Strasse ein';
$e[8]='Bitte geben Sie Ihre Hausnummer ein';
$e[11]='Die Vorwahl ist zu kurz';
$e[12]='Die Rufnummer ist zu kurz';
$e[13]='Die Vorwahl ist zu lang';
$e[14]='Die Rufnummer ist zu lang';
$e[15]='Der Vorname ist zu kurz';
$e[16]='Der Nachname ist zu kurz';
$e[17]='Der Vorname ist zu lang';
$e[18]='Der Nachname ist zu lang';
$e[20]='Wenn Sie ausserhalb der Gesch&auml;ftszeiten bestellen, m&uuml;ssen Sie im Feld Vorbestellung Angaben zum Liefertermin machen.';
$e[101]='Ort und PLZ geh&ouml;ren nicht zum Liefergebiet';


##############################################################################
##############################################################################
?>