<?php 
##############################################################################
##############################################################################

function safeipt($get){
$get=htmlspecialchars(strip_tags($get)); 
$get=preg_replace("/(\[url|content-type:|bcc:|cc:|to:|from:)/im", "",$get);
$get=trim($get); return $get;}


function meldungen(){
$gettit='';
$meldung='';

if(isset($_GET['t'])){$gettit=safeipt($_GET['t']);}
if(isset($_GET['tell'])){$gettit=safeipt($_GET['tell']);}

if($gettit==""){return $meldung;}


$meldungen_arr=array(

/* Farbe entsprechend der Meldung (Fehler=Rot #990000 , Erfolg=Grn #009900) */

"1" => array(0 => 'Der Artikel wurde angelegt', 1 => '#009900'),
"2" => array(0 => 'Der Artikel wurde ge&auml;ndert', 1 => '#009900'),
"3" => array(0 => 'Der Artikel wurde gel&ouml;scht', 1 => '#009900'),
"4" => array(0 => 'Das Liefergebiet wurde angelegt', 1 => '#009900'),
"5" => array(0 => 'Das Liefergebiet wurde ge&auml;ndert', 1 => '#009900'),
"6" => array(0 => 'Das Liefergebiet wurde gel&ouml;scht', 1 => '#009900'),

"7" => array(0 => 'Die &Ouml;ffnungszeiten wurden gespeichert', 1 => '#009900'),
"8" => array(0 => 'Die &Ouml;ffnungszeiten wurden gespeichert', 1 => '#009900'),

"9" => array(0 => 'Das Artikel-Foto wurde gel&ouml;scht', 1 => '#009900'),
"10" => array(0 => 'Upload erfolgreich', 1 => '#009900'),
"11" => array(0 => 'Die Zutat wurde angelegt', 1 => '#009900'),
"12" => array(0 => 'Die Zutat wurde ge&auml;ndert', 1 => '#009900'),
"13" => array(0 => 'Die Zutat wurde gel&ouml;scht', 1 => '#009900'),
"14" => array(0 => 'Der Zusatzstoff wurde angelegt', 1 => '#009900'),
"15" => array(0 => 'Der Zusatzstoff wurde ge&auml;ndert', 1 => '#009900'),
"16" => array(0 => 'Der Zusatzstoff wurde gel&ouml;scht', 1 => '#009900'),
"17" => array(0 => '&Auml;nderungen gespeichert', 1 => '#009900'),
"18" => array(0 => 'Der Kunde wurde gel&ouml;scht', 1 => '#009900'),
"19" => array(0 => 'Die Rubrik wurde angelegt', 1 => '#009900'),
"20" => array(0 => 'Die Rubrik wurde ge&auml;ndert', 1 => '#009900'),
"21" => array(0 => 'Die Rubriken wurden umpositioniert', 1 => '#009900'),
"22" => array(0 => 'Die Rubrik wurde gel&ouml;scht', 1 => '#009900'),
"23" => array(0 => 'Die Warengruppe wurde angelegt', 1 => '#009900'),
"24" => array(0 => 'Die Warengruppe wurde ge&auml;ndert', 1 => '#009900'),
"25" => array(0 => 'Die Warengruppe wurde gel&ouml;scht', 1 => '#009900'),
"26" => array(0 => 'Die Zutatenliste wurden angelegt', 1 => '#009900'),
"27" => array(0 => 'Die Zutatenliste wurde ge&auml;ndert', 1 => '#009900'),
"28" => array(0 => 'Die Zutatenliste wurde gel&ouml;scht', 1 => '#009900'),

"29" => array(0 => 'Konfiguration gespeichert', 1 => '#009900'),
"30" => array(0 => 'Die Bewertung wurde gel&ouml;scht', 1 => '#009900'),
    "31" => array(0 => 'Das Foto wurde gel&ouml;scht', 1 => '#009900'),
    "32" => array(0 => 'Die Bestellung wurde gel&ouml;scht', 1 => '#009900'),


"401" => array(0 => 'Uploadfehler: keine g&uuml;ltige Dateiendung', 1 => '#990000'),
"402" => array(0 => 'Uploadfehler: Der Mime-Typ der Datei ist fehlerhaft oder wird nicht unterst&uuml;tzt.', 1 => '#990000'),
"403" => array(0 => 'Beim Upload ist ein Fehler aufgetreten.', 1 => '#990000'),
"404" => array(0 => 'Beim Erstellen des Thumbnails ist ein Fehler aufgetreten.', 1 => '#990000'),

"444" => array(0 => 'Konfiguration konnte nicht gespeichert werden', 1 => '#990000'),

"9999" => array(0 => 'test!', 1 => '#990000')
);



$drumherum='<table class="selection" cellpadding="10" style="width:500px;border:2x solid black;"><tr><td><b>[PLATZHALTER]</b></td></tr></table><br><br>';

$numbers=explode(",", $gettit);
$countit=count($numbers);

for($i=0; $i<$countit; $i++){
$meldung.='<p style="color:'.$meldungen_arr[$numbers[$i]][1].';">'.$meldungen_arr[$numbers[$i]][0].'</p>';
}

$meldungsbox = preg_replace("=\[PLATZHALTER\]=", $meldung, $drumherum);

return $meldungsbox;
}


/* meldungentester.php?t=101,405 */

##############################################################################
##############################################################################
?>