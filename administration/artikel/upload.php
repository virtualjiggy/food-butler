<?php
##############################################################################
##############################################################################

error_reporting(0);

@session_start();
if($_SESSION['admin']!="ontime"){
    header("Location: ../login.php");
    exit();
}

##############################################################################
##############################################################################

define('IMSCRIPT', '1');
@require("../inclx/db_vbdg.php");
@require("../inclx/funcx.php");
@require("../inclx/meldungen.php");





##############################################################################
##############################################################################

$aid=(int)$_POST['id'];
$feldnames[0]='uploadfile';


$upload_limitieren = 0;      // moegliche Werte 0 und 1 (aus/an)
$upload_groesse = 550;       // in Kilobyte


##| Auch die Datei-Endungen und Mime_Typen der Dateien koennen ein-
##| geschraenkt werden. Dafuer stehen die folgenden Array zur
##| Verfuegung.

$erlaubte_dateiendungen = array('gif','jpg','png');
$erlaubte_mimetypen = array("image/pjpg","image/pjpeg","image/gif","image/jpg","image/png", "image/jpeg");


$e[0]="Uploadfehler: Die erlaubte Dateigroesse $upload_groesse kb ist ueberschritten";
$e[1]="Uploadfehler: Die Dateiendung ist nicht erlaubt.";
$e[2]="Uploadfehler: Der Mime-Typ der Datei ist fehlerhaft oder wird nicht unterstuetzt.";
$e[3]="Uploadfehler: unbestimmter Fehler aufgetreten";


##############################################################################
##############################################################################


$zielordner = '../../produktbilder/';    // ggf. Schreibrechte (CHMOD) erteilen!


$datum=date("d.m.Y");
$uhrzeit=date("H:i");
$ausgabe='';
$ausgabe_2='';

##############################################################################
##############################################################################


if(is_array($feldnames)){
    foreach($feldnames as $feldname){
        if(!empty($_FILES[$feldname]['name'])){

            $mime = '';
            $endung = '';
            $endung_mp = '';

            if(!$_FILES[$feldname]['error']){

                $mime = $_FILES[$feldname]['type'];
                $endung = strrchr($_FILES[$feldname]['name'],'.'); // sucht das letzte Vorkommen e. Punktes
                $endung_mp = strtolower($endung); // Endung mit Punkt
                $endung = str_replace(".", "", strtolower($endung)); // Punkt weg

                $prueffehler=0;
                ## Pruefung ob erlaubte Dateiendung okay geht
                if(!in_array($endung, $erlaubte_dateiendungen)){
                    $fehler=1;
                    $prueffehler++;
                    $ausgabe.=$e[1].' </span><br>';
                }

                ## Pruefung ob erlaubter Mime okay geht
                if(!in_array($mime, $erlaubte_mimetypen)){
                    $fehler=1;
                    $prueffehler++;
                    $ausgabe.=$e[2].' </span><br>';
                }

                ## Pruefung auf Dateigroesse, falls gefordert
                if($upload_limitieren==1){ //Limitierung eingeschaltet
                    if($_FILES[$feldname]['size'] > (1000*$upload_groesse)){
                        $fehler=1;
                        $prueffehler++;
                        $ausgabe.=$e[0].' </span><br>';
                    }}

                if($prueffehler==0){$ausgabe_2.='Upload der Datei: scheint moeglich</span><br>';}

            }else{
                $fehler=1;
                $ausgabe.=$e[3].'</span><br>';
            }
        }

    }



    // wenn Fehler, dann hier stopp machen und Fehler ausgeben
    if(isset($fehler)){
        $err=1;
        $needed_err.= $ausgabe_2.'<br>';
        $needed_err.= $ausgabe.'<br>';
        $needed_err.='Der Uploadvorgang wurde abgebrochen<br>';
        #echo $back;
        #exit();
        $ausgabe='';
        $ausgabe_2='';
    }



    // wenn es bishier keinen fehler gab, stimmt die dateigr und die endung und das mimetyp
    // fehlt nur noch der eigentliche Upload
    if(!isset($fehler)){
        foreach($feldnames as $feldname){

            $uploadresult='';

            if(!empty($_FILES[$feldname]['name']) && !$_FILES[$feldname]['error']){


                $uploadresult=dotheupload($feldname, $zielordner, $e);

                ## das dotheupload spuckt Array aus
                ## [1]  wert 0 = Upload nicht erfoglreich   wert 1= Upload erfolgreich
                ## [2]  pfad und dateiname
                ## [4]  Dateiendung (ohne Punkt)
                ## [5]  Original Dateiname
                ## [6]  Ziel Dateiname


                if($uploadresult[1]==0){ // Upload fehlerhaft
                    $ausgabe.='Upload fehlgeschlagen: '.$uploadresult[5].'<br>';

                    $err=1; echo $ausgabe;

                }else{


                    $art_img=$uploadresult[6].'.'.$uploadresult[4];

                    $src=$uploadresult[2];
                    $dst='../../produktbilder/'.$uploadresult[6].'_th.'.$uploadresult[4]; // wird angelegt

                    $dst_db=$uploadresult[6].'_th.'.$uploadresult[4];  



                    prepare($src, '75', $dst);

                    mysql_query(" UPDATE vob_artikel SET  art_img='$art_img', art_img_th='$dst_db'  WHERE id='$aid' ");






                    $zieldateinamemitendung= $uploadresult[6].'.'.$uploadresult[4];
                    $ausgabe.='Upload erfolgreich: '.$uploadresult[5].'<br>';
                }
            }
        }
    }



##############################################################################
##############################################################################

} //is_array feldnames


##############################################################################
##############################################################################


    @header("Location: details.php?id=$aid");


##############################################################################
##############################################################################

function dotheupload($feldname, $zielpfad, $e=array()){

    $endung='';
    $endung_mp='';
    $orginalname='';
    $zielname='';
    $realezielname='';
    $pfile='';
    $errnumbers='';
    $result='';

    $endung = strrchr($_FILES[$feldname]['name'],'.'); // sucht das letzte Vorkommen e. Punktes
    $endung_mp = strtolower($endung); // Endung mit Punkt
    $endung = str_replace(".", "", strtolower($endung)); // Punkt weg

    $orginalname=$_FILES[$feldname]['name'];

    $zielname=str_replace($endung_mp, "", $_FILES[$feldname]['name']);
    $zielname=str_replace(" ", "_", $zielname);
    $zielname= strtolower(preg_replace('/[^a-zA-Z0-9_.+-]/', '', $zielname)); // Sonderzeichen rausfiltern

    $pfile=$zielpfad.$zielname.'.'.$endung;
    $realezielname=$zielname;

## falls vorhanden, suffix dran, wieder pruefen
    $i=0;
    while(file_exists($pfile)){
        $pfile=$zielpfad.$zielname.'_'.$i.'.'.$endung;
        $realezielname=$zielname.'_'.$i;
        $i++;
    }

## uploadvorgang
    @move_uploaded_file($_FILES[$feldname]['tmp_name'], $pfile);
    @chmod($pfile, 0644);

## datei exisitert nicht?
    if(!file_exists($pfile)){ $fehler=1;}

## wenn Datei exitiert, aber Dateigroesse 0Byte, dann wegloeschen und ERROR ausgeben.
    if(get_size($pfile)=="0" && $_FILES[$feldname]['size']!="0"){@unlink($pfile); $fehler=1;}




    // wenn bis hierher kein Fehler dann alles roger!
    if(!isset($fehler)){

        $result=1;
        #session_unset();

    }else{

        $result=0;

    }



    $ergebnisse=array(
        '1' => $result,
        '2' => $pfile,
        '4' => $endung,
        '5' => $orginalname,
        '6' => $realezielname,
        '7' => $zielname.'.'.$endung
    );


    return $ergebnisse;

} //endfunction





###############################################################################
###############################################################################

function get_size($path)
{
    $size='';
    if(!is_dir($path))return @filesize($path);
    $dir = opendir($path);
    while($file = readdir($dir))
    {
        if(is_file($path."/".$file))$size+=@filesize($path."/".$file);
        if(is_dir($path."/".$file) && $file!="." && $file !="..")$size +=get_size($path."/".$file);
    }
    return $size;
}


##############################################################################
##############################################################################

function GrafikSkalieren($Grafik, $Seite)
{
    $Bilddaten = getimagesize($Grafik);
    $Breite = $Bilddaten[0];
    $Hoehe  = $Bilddaten[1];
    $Format = $Bilddaten[2];

    if($Breite > $Hoehe)
    {
        $NeueBreite = $Seite;
        $NeueHoehe = $Seite / $Breite * $Hoehe;
    }
    else
    {
        $NeueBreite = $Seite / $Hoehe * $Breite;
        $NeueHoehe = $Seite;
    }

    $SkalierteGrafik = ImageCreateTrueColor($NeueBreite, $NeueHoehe);

if($Format == 1){$Originalgrafik = ImageCreateFromGIF($Grafik);}
if($Format == 2){$Originalgrafik = ImageCreateFromJPEG($Grafik);}
if($Format == 3){$Originalgrafik = ImageCreateFromPNG($Grafik);}

    ImageCopyResampled($SkalierteGrafik, $Originalgrafik, 0, 0, 0, 0, $NeueBreite, $NeueHoehe, $Breite, $Hoehe);
    return $SkalierteGrafik;
}


function prepare($datei, $ausgabegroesse, $destfile){
$dform=@getimagesize($datei);
## je nach Format den Header ausw�hlen und Grafik erzeugen Funktion aufrufen
if($dform[2]==1){ ImageGIF(GrafikSkalieren($datei, $ausgabegroesse), $destfile, 100);}
if($dform[2]==2){ImageJPEG(GrafikSkalieren($datei, $ausgabegroesse), $destfile, 100);}
if($dform[2]==3){ ImagePNG(GrafikSkalieren($datei, $ausgabegroesse), $destfile, 100);}


}

##############################################################################
##############################################################################
 

