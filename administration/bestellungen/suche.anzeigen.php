<?php 
##############################################################################
##############################################################################

    error_reporting(0);

    @session_start();
    if($_SESSION['admin']!="ontime"){
    header("Location: ../login.php");
    exit();
    }

##############################################################################
##############################################################################

    define('IMSCRIPT', '1');
    @require("../inclx/db_vbdg.php");
    @require("../inclx/config.php");
    @require("../inclx/funcx.php");
    @require("../inclx/meldungen.php");



function mach_Timestamp($datum,$tag_dazu=0){

    $x   = explode(".", $datum);

    $mkt = mktime(0,0,0,$x[1],$x[0]+$tag_dazu,$x[2]);

        return $mkt;


}
##############################################################################
##############################################################################


$von=mig($_POST['von']);
$bis=mig($_POST['bis']);

$von_ts=mach_Timestamp($von);
$bis_ts=mach_Timestamp($bis,1);


##############################################################################
##############################################################################
?>

<!DOCTYPE HTML>
<html>
<head>

    <title>vioCMS Administration - vionlink OBS</title>
    <meta http-equiv="content-type" content="text/html; charset=<?php echo $SCHRIFTSATZ; ?>">
    <link rel="stylesheet" type="text/css" href="mas.layout.css">

    <script type="text/javascript" src="../js/jquery-1.3.2.min.js"></script>
    <?php if(file_exists("../js/jquery-ui.min.js")){ ?>
        <link rel="stylesheet" href="../js/jquery-ui.css" type="text/css">
        <script type="text/javascript" src="../js/jquery-ui.min.js"></script>
        <script type="text/javascript">
            $(function(){$('.dateselector').datepicker({ dateFormat: 'dd.mm.yy' });});
        </script>
        <style type="text/css">
            #ui-datepicker-div{width:300px;}
            .ui-datepicker .ui-datepicker-prev span {border:1px solid #fff;background:url(../js/arrow_left.jpg) no-repeat;}
            .ui-datepicker .ui-datepicker-next span {border:1px solid #fff;background:url(../js/arrow_right.jpg) no-repeat;}
            .selection{width:600px;}
            .rt td{text-align: right;}

            <?php
if($_POST['print']==1){
echo '.noprint{display:none;}';
}
 ?>

        </style>
    <?php } ?>

    <link rel="stylesheet" type="text/css" href="../styling.css" />



</head>

<?php
if($_POST['print']==1){
    echo '<body onload="window.print()">';
}else{
    echo '<body>';
}
?>




<a class="noprint" href="suchen.php">zurück</a>
<br class="noprint" >
<br class="noprint" >
<br class="noprint" >
<br class="noprint" >


Bestellungen im Zeitraum: <b><?php echo $von; ?></b> - <b><?php echo $bis; ?></b>
<br>
<br>


<table border=1 cellspacing=2 cellpadding=2>
    <tr>
        <td>Position</td>
        <td>Datum</td>
        <td>Zwischensumme</td>
        <td>Anfahrt</td>
        <td>Summe</td>
        <td>&nbsp;&nbsp;USt.()&nbsp;&nbsp;</td>
        <td>&nbsp;&nbsp;USt.()&nbsp;&nbsp;</td>
        <td>&nbsp;&nbsp;USt.()&nbsp;&nbsp;</td>
        <td>Anfahrt USt.</td>
     </tr>

        <?php
$i=1;


$r=mysql_query(" SELECT * FROM  vob_bestellungen   WHERE bestelltime >='$von_ts' AND bestelltime < '$bis_ts' ");
while($res=mysql_fetch_object($r)){


    echo '<tr class="rt">';
    echo '<td>'.$i.'</td>';
    echo '<td>'.date("d.m.Y", $res->bestelltime).'</td>';
    echo '<td>'.$res->zwischensumme.'</td>';
    echo '<td>'.$res->anfahrt.'</td>';
    echo '<td>'.$res->summe.'</td>';
    echo '<td>'.$res->ust1.'</td>';
    echo '<td>'.$res->ust2.'</td>';
    echo '<td>'.$res->ust3.'</td>';
    echo '<td>'.$res->anfahrt_ust.'</td>';
    echo '</tr>';
    $i++;

    $gesamt_zwischensumme = round($gesamt_zwischensumme + $res->zwischensumme,2);
    $gesamt_anfahrt = round($gesamt_anfahrt + $res->anfahrt,2);
    $gesamtsumme = round($gesamtsumme + $res->summe,2);
    $gesamt_ust1 = round($gesamt_ust1 + $res->ust1,2);
    $gesamt_ust2 = round($gesamt_ust2 + $res->ust2,2);
    $gesamt_ust3 = round($gesamt_ust3 + $res->ust3,2);
    $gesamt_anfahrt_ust = round($gesamt_anfahrt_ust + $res->anfahrt_ust,2);

}
    ?>

    <tr><td colspan="7">&nbsp;</td></tr>
    <tr class="rt">
        <td colspan="2">&nbsp;<i>gesamt</i></td>
        <td><?php echo $gesamt_zwischensumme; ?></td>
        <td><?php echo $gesamt_anfahrt; ?></td>
        <td><?php echo $gesamtsumme; ?></td>
        <td><?php echo $gesamt_ust1; ?></td>
        <td><?php echo $gesamt_ust2; ?></td>
        <td><?php echo $gesamt_ust3; ?></td>
        <td><?php echo $gesamt_anfahrt_ust; ?></td>
    </tr>


</table>

<br>
<br>

<form method="post">
    <input type=hidden name="print" value="1">
    <input type=hidden name="von" value="<?php echo $von; ?>">
    <input type=hidden name="bis" value="<?php echo $bis; ?>">
    <input type="submit" name="drucken" value="drucken" class="noprint">
</form>



    <br>
    <br>
<p class="noprint">
    * Die Summen für USt., Zwischensumme, Anfahrt werden erst ab
    <br>    vionlink OBS 3.2 mit diesem Auszug angezeigt (und die Gesamtzahlen ermittelt)</p>