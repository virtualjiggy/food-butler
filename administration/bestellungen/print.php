<?php 
##############################################################################
##############################################################################

error_reporting(0);

##############################################################################
##############################################################################

@session_start();
if($_SESSION['admin']!="ontime"){
    header("Location: ../login.php");
    exit();
}

##############################################################################
##############################################################################

define('IMSCRIPT', '1');
@require("../inclx/db_vbdg.php");
@require("../inclx/config.php");
@require("../inclx/funcx.php");
@require("../inclx/meldungen.php");
header("Content-Type: text/html; charset=$SCHRIFTSATZ");

##############################################################################
##############################################################################
?>


    <!DOCTYPE HTML>
<html>
    <head>

        <title>Restaurant Zaza Kebap</title>
        <meta http-equiv="content-type" content="text/html; charset=<?php echo $SCHRIFTSATZ; ?>">
        <link rel="stylesheet" type="text/css" href="mas.layout.css">



        <link rel="stylesheet" type="text/css" href="../styling.css" />



    </head>
<body onload="window.print(); ">


<?php
##############################################################################
##############################################################################


// Bestell ID
$BID=mig($_GET['id']);

##############################################################################
##############################################################################




$r=mysql_query(" SELECT * FROM vob_bestellungen WHERE id='$BID' ");
while($res=mysql_fetch_object($r)){
    $to_kunde =$res->to_kunde;
    $to_lieferanschrift=$res->to_lieferanschrift;
    $summe =$res->summe;
    $bestellung =$res->bestellung;
    $bestelltime=$res->bestelltime;
    $s=$res->series;
    $se=$res->series_e;
}

$r1=mysql_query(" SELECT * FROM vob_kunden WHERE id='$to_kunde' ");
$r2=mysql_query(" SELECT * FROM vob_lieferanschriften WHERE id='$to_lieferanschrift' ");






while($res1=mysql_fetch_object($r1)){
$anrede =$res1->anrede;
$name =$res1->name;
$vorname =$res1->vorname;
$mail =$res1->email;
$newsletter =$res1->newsletter;
$since =$res1->since;
}

while($res2=mysql_fetch_object($r2)){
$strasse=$res2->strasse;
$plz=$res2->plz;
$ort=$res2->ort;
$vorwahl=$res2->vorwahl;
$rufnummer=$res2->rufnummer;
$nummer=$res2->nummer;
$firma=$res2->firma;
$abteilung=$res2->abteilung;
$besonderheit=$res2->besonderheit;
}




##############################################################################
##############################################################################
?>




<table class="selectionprint" style="width:600px;">
<tr><td class="spec" colspan="3"><b>Bestellung <?php echo $BID; ?></b></td></tr>


    <?php

    //wenn XML dann
    if(strpos($bestellung, '<bestellung>')  === false){
    
    // pruefen ob html oder ganz altes unformatiertes zeug
      if(strpos($bestellung, '<tr>')  !== false){
       echo $bestellung;
      }else{
         echo '<tr><td>'.nl2br($bestellung).'</td></tr>';
      }
    

    }else{
     

        $XML=$bestellung;
        
        $xmlObj = simplexml_load_string($XML);
          if($xmlObj==false){
            $XML =utf8_encode($XML);
            $xmlObj = simplexml_load_string($XML);
          } 

         
        $arrXml = objectsIntoArray($xmlObj);




        $htmlmail=' <tr><td colspan="3">
        <br>Bestellung vom '.date("d.m.Y").'  '.date("H:i").' Uhr<br><br>
        </td></tr>

    <tr>
        <td class="spec"> </td>
        <td class="spec"><b>Produkte</b></td>
        <td class="spec" align="right"><b>Preis</b></td>
    </tr>
    <tr><td colspan="3">&nbsp;</td></tr>';






        foreach($arrXml[articlelist][article] as $k => $v){

            if(is_numeric($k)){

                $htmlmail.= "<tr>";
                $htmlmail.= "<td valign=top><b>".$arrXml[articlelist][article][$k][amount]."</b></td>";
                $htmlmail.= "<td><b>".$arrXml[articlelist][article][$k][name]."</b>";

                if(!empty($arrXml[articlelist][article][$k]['gratiszutaten'])){
                    $htmlmail.= "<br>".$arrXml[articlelist][article][$k]['gratiszutaten'];
                }

                if(!empty($arrXml[articlelist][article][$k]['extrazutaten'])){
                    $htmlmail.= "<br>".$arrXml[articlelist][article][$k]['extrazutaten'];
                }

                $htmlmail.= "</td>";
                $htmlmail.= "<td align=right>".$arrXml[articlelist][article][$k]['price']." <?php echo $WAEHRUNG_SYM; ?></td>";
                $htmlmail.= "</tr>";

            }else{ $NO_FOREACH=1;

            }
        }

        if($NO_FOREACH==1){


            $htmlmail.= "<tr>";
            $htmlmail.= "<td valign=top><b>".$arrXml[articlelist][article][amount]."</b></td>";
            $htmlmail.= "<td><b>".$arrXml[articlelist][article][name]."</b>";

            if(!empty($arrXml[articlelist][article]['gratiszutaten'])){
                $htmlmail.= "<br>".$arrXml[articlelist][article]['gratiszutaten'];
            }

            if(!empty($arrXml[articlelist][article]['extrazutaten'])){
                $htmlmail.= "<br>".$arrXml[articlelist][article]['extrazutaten'];
            }

            $htmlmail.= "</td>";
            $htmlmail.= "<td align=right>".$arrXml[articlelist][article]['price']." ".$WAEHRUNG_SYM."</td>";
            $htmlmail.= "</tr>";


        }




        $htmlmail.='<tr><td colspan=3>&nbsp;</td></tr>
        <tr><td colspan="2" align="right">&nbsp;Zwischensumme</td>
            <td align="right">'.$arrXml["zwischensumme"].' '.$WAEHRUNG_SYM.'</td></tr>
        <tr><td colspan="2" align="right">&nbsp;Anfahrt</td>
            <td align="right">'.$arrXml['anfahrt'].' '.$WAEHRUNG_SYM.'</td></tr>
        <tr><td colspan="2" align="right">&nbsp;Gesamtsumme</td>
            <td align="right">'.$arrXml['gesamtsumme'].' '.$WAEHRUNG_SYM.'</td></tr>
    <tr><td colspan=3>&nbsp;</td></tr>
 ';


        echo $htmlmail;


    }



    ?>
    <tr><td class="spec" colspan=3> </td></tr>
</table>

    <br><br>


<table class="selectionprint" style="width:600px;">
    <tr><td colspan="2" class="spec"> Kunde</td></tr>
    <tr><td colspan="2">&nbsp;</td></tr>




    <tr><td style="width:180px;">Anrede</td><td style="width:420px;"><?php echo $anrede; ?></td></tr>
    <tr><td>Name</td><td><?php echo $name; ?></td></tr>
    <tr><td>Vorname</td><td><?php echo $vorname; ?></td></tr>
    <tr><td>Email</td><td><?php if($mail==""){echo'-';}else{echo $mail;} ?></td></tr>
    <tr><td>angemeldet seit</td><td><?php if($since=="" || $since=="0"){echo'-';}else{echo @date("d.m.Y", $since);} ?></td></tr>
    <tr><td>Newsletter-Abo</td><td><?php if($newsletter=="1"){echo'ja';}else{echo 'nein';} ?></td></tr>

    <tr><td colspan="2">&nbsp;</td></tr>

    <tr><td>Vorwahl</td><td><?php echo $vorwahl; ?></td></tr>
    <tr><td>Rufnummer</td><td><?php echo $rufnummer; ?></td></tr>
    <tr><td>Strasse</td><td><?php echo $strasse; ?> <?php echo $nummer; ?></td></tr>
    <tr><td>PLZ / Ort</td><td><?php echo $plz; ?> <?php echo $ort; ?></td></tr>
    <tr><td>Firma</td><td><?php echo $firma; ?></td></tr>
    <tr><td>Abteilung</td><td><?php echo $abteilung; ?></td></tr>
    <tr><td>Besonderheit </td><td><?php echo $besonderheit; ?></td></tr>

    <tr><td colspan="2">&nbsp;</td></tr>

<tr><td colspan="2" align="right">&nbsp;<br>

</td></tr><tr><td colspan="2" class="spec">&nbsp;</td></tr>
</table>
<br><br>





<?php


#$_SESSION['bestellschein'] = unserialize($s);
#$_SESSION['extras'] = unserialize($se);

#print_r($_SESSION['bestellschein']);
#print_r($_SESSION['extras']);



#######################################################################################
#######################################################################################


function objectsIntoArray($arrObjData, $arrSkipIndices = array())
{
    $arrData = array();

// if input is object, convert into array
    if (is_object($arrObjData)) {
        $arrObjData = get_object_vars($arrObjData);
    }

    if (is_array($arrObjData)) {
        foreach ($arrObjData as $index => $value) {
            if (is_object($value) || is_array($value)) {
                $value = objectsIntoArray($value, $arrSkipIndices); // recursive call
            }
            if (in_array($index, $arrSkipIndices)) {
                continue;
            }
            $arrData[$index] = $value;
        }
    }
    return $arrData;
}

#######################################################################################
#######################################################################################


function read_groesse($productID,$productprice){

$wq=@mysql_query(" SELECT preis1,preis2,preis3,preis4,to_warengruppe FROM vob_artikel WHERE id='$productID'  ");
while($red=@mysql_fetch_object($wq)){
$dbpreis1=$red->preis1;
$dbpreis2=$red->preis2;
$dbpreis3=$red->preis3;
$dbpreis4=$red->preis4;
$dbto_warengruppe=$red->to_warengruppe;
}

$wq=@mysql_query(" SELECT pe_str FROM vob_warengruppen WHERE id='$dbto_warengruppe'  ");
while($red=@mysql_fetch_object($wq)){
$strX=explode("|", $red->pe_str);
}

if($productprice==$dbpreis1){$groesseneinheit=$strX[0];}
if($productprice==$dbpreis2){$groesseneinheit=$strX[1];}
if($productprice==$dbpreis3){$groesseneinheit=$strX[2];}
if($productprice==$dbpreis4){$groesseneinheit=$strX[3];}

return $groesseneinheit;
}

#######################################################################################
#######################################################################################
?>