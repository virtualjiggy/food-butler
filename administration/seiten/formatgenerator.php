<?php 
##############################################################################
##############################################################################

$BLEND='slayer';
@require("../skeleton.php");

error_reporting(0);

##############################################################################
##############################################################################


if(isset($_POST['submit'])){

$body_bg=str_replace("#", "", $_POST[body_bg]);
$text_clr=str_replace("#", "", $_POST[text_clr]);
$text_font=$_POST[text_font];
$font_size=$_POST[font_size];
$container_bg=str_replace("#", "", $_POST[container_bg]);
$container_borderradius=$_POST[container_borderradius];
$wgtab_bg=str_replace("#", "", $_POST[wgtab_bg]);
$wgtabhead_bg=str_replace("#", "", $_POST[wgtabhead_bg]);
$wgtab_bg_o=str_replace("#", "", $_POST[wgtab_bg_o]);
$wgtab_bg_u=str_replace("#", "", $_POST[wgtab_bg_u]);
$wgtab_clr=str_replace("#", "", $_POST[wgtab_clr]);
$contenbox_bg=str_replace("#", "", $_POST[contenbox_bg]);
$instance_color=str_replace("#", "", $_POST[instance_color]);



if($body_bg==""){
$FORMATCSS='';
$body_bg_4a='000';
}else{
$FORMATCSS='body{background:#'.str_replace("#", "", $body_bg).';}';
$body_bg_4a=$body_bg;
}


$FORMATCSS.='
body{color:#'.str_replace("#", "", $_POST[text_clr]).';}
body{font-family:'.$_POST[text_font].',sans-serif;}
body{font-size:'.$_POST[font_size].'px;}
.container{background:#'.str_replace("#", "", $_POST[container_bg]).';}
.container{border-radius:'.$_POST[container_borderradius].'px;}
.warengruppen_tabelle{background:#'.str_replace("#", "", $_POST[wgtab_bg]).';}

.warengruppen_tabelle .th{background:#'.str_replace("#", "", $_POST[wgtabhead_bg]).';}

.t_bestellschein .th{background:#'.str_replace("#", "", $_POST[wgtabhead_bg]).';}
#contenttable .tb{background:#'.str_replace("#", "", $_POST[wgtabhead_bg]).';}




.odd{background:#'.str_replace("#", "", $_POST[wgtab_bg_o]).';} 
.uneven{background:#'.str_replace("#", "", $_POST[wgtab_bg_u]).';}
.warengruppen_tabelle{color:#'.str_replace("#", "", $_POST[wgtab_clr]).';}
.contentbox{background-color:#'.str_replace("#", "", $_POST[contenbox_bg]).';}

.instance_color{background:#'.str_replace("#", "", $_POST[instance_color]).';}  

#produktnavigation ul a {#'.str_replace("#", "", $_POST[text_clr]).';}
#produktnavigation ul a:hover{color:#'.$body_bg_4a.';}
#mainnavigation a{color:#'.str_replace("#", "", $_POST[text_clr]).';}
#mainnavigation a:hover{color:#'.$body_bg_4a.';}
#footer a{color:#'.str_replace("#", "", $_POST[text_clr]).';}
#footer a:hover{color:#'.$body_bg_4a.';}


 
 
/* Speisekarte Linkfarbe  */ 
#speisekarte ul a{color:#222;text-decoration:none;}
#speisekarte ul a:hover{color:#BE0000;background-color:#FFEB99;}


/* Liefergebiete Liste  */
.l{float:left;width:260px;}
.r{float:left;width:260px;}


/* Hintergrundfarbe Bestellschein */
.bestellschein_tabelle{max-width:300px;   padding-left: 10px;
    padding-top: 14px;}


/* Hintergrund, Schriftfarbe und Rahmen der Hinweisbox */
.messageinner{background:#ffffff; border:1px solid #cccccc; color:#000000;}


/* Warengruppe Tabelle Link/Produktname  */
.produkt{color:#551A8B;text-decoration:none;}

/* Warengruppe Textfarbe Rubrikbeschreibung  */
.desc{color:#292929;}


/* Detailseite Tabelle Hintergrund */
.p_details .tablebody{background-color:#FFEB99;}

/* Detailseite Tabelle  */
table.p_details {font-size:12px;background-color:#FFEB99;}

/* Detailseite Tabelle Titelzeile Hintergrund */
table.p_details .th{background-color:#FF8800;color:#292929;font-size:12px;}

/* Links in Tabelle Extrazutaten, Inklusivzutaten  */
table.p_details a {color:#F53900;text-decoration:none;font-size:11px;}


table.p_details a b{color:#292929;text-decoration:none;font-size:13px;}
table.p_details a:hover b{color:#FFD35C;text-decoration:none;background:#FF8800;}



/* Fehlermeldung im Bestellformular  Rahmen, Farbe und Breite */
.fehlermeldung{border:2px solid red;color:red;width:400px;}
.hinweismeldung{border:2px solid green;color:green;width:400px;}


/* ############################################### */
/* ############################################### */


#produktnavigation ul a {font-size:12px;text-decoration:none;}
#produktnavigation ul a:hover{background-color:#FFEB99;}
#mainnavigation a{font-size:18px;text-decoration: none;}
#footer a{text-decoration: none;}



#content{background-color:#fff;}

#mainnavigation{border:1px solid #000;border-radius:6px;}
#mainnavigation a{font-size:18px;text-decoration: none;}
#mainnavigation a:hover{}

#designhelper{background-color:#fff;}
#topping{background:url(img/topping.png) repeat-x;}


a{color:#BE0000;}
a.nachoben{font-size:10px;color:#fff;}

.footerinner{border-radius:6px;border:1px solid #000;border-top: 0px;}
#footer a{text-decoration: none;}

.info {font-size:12px;}
.info a{font-size:12px;}


.sidebox{border-radius:8px;}
.sidebox h2{font-size:18px;}


#produktnavigation ul a{font-size:12px;color:#222;text-decoration:none;}
#produktnavigation ul a:hover{color:#BE0000;background-color:#FFEB99;}
#contenttable{background-color:#FFD35C;font-size:12px;color:#292929;}


.t_bestellschein{font-size:11px;}

.t_bestellschein2{background:transparent;font-size:11px;}
.t_bestellschein a.z_btn{background-color:#FF8800;font-size:9px;text-decoration:none;color:#292929;border:1px solid #FF8800;}
.t_bestellschein a.wkz {color:#F53900;text-decoration:none;}
.t_bestellschein a.wkz:visited {color:#F53900;text-decoration:none;}
.t_bestellschein .hidden{background-color:#FFEB99;}
#content h2{font-size:22px;}


/* ############################################### */
/* ############################################### */

/* ############################################### */
/* ############################################### */



.rightbox dt{margin:0;padding:0;float:left;width:110px;}
.rightbox dl{margin:0;padding:0;clear:left;}

.cl{clear:left;}
img{border:0;}
#p_t{padding-top:12px;padding-left:12px;}

#liefergebiete{max-width:650px;min-height:500px;}
#liefergebiete dl{margin:0;padding:0;min-height:500px;padding-left:40px;padding-top:20px;}
#liefergebiete ol{margin:0;padding:0;min-height:500px;padding-left:40px;padding-top:20px;}

.warengruppen_tabelle{width: 100%;}

.articleimg{position:relative;}
.article_thumbnail{float:left;padding-top:5px;padding-right:6px;}

.bildbox{position:absolute;top:10px;left:100px;}
.bildbox{display:none;}


#speisekarte{float:left;padding-right:40px;padding-left:60px;}
/* #bestellschein{float:left;} */

#speisekarte ul{float:left; text-align:left;  margin:0; padding:0;    margin: 0 12px 0 0;    padding: 10px 0 0 40px;    max-width: 150px;}
#speisekarte li{line-height:20px;}
#speisekarte ul a{width:145px;padding:4px;cursor:pointer;}


.fehlermeldung{text-align:left;padding:10px;}

';


$FORMATCSS.="

#messagebox{position:absolute;left:0;top:50px;display:none;}
.messageinner{width:450px;min-height:200px;margin:0;padding:0;text-align:left;}
.message{padding:20px;width:400px;}
.messageinner span{position:relative;display:block;background:#FF8800;padding:4px;font-weight:bold;}
.messageinner a.hdl{position:absolute;top:3px;right:6px;width:18px;height:18px;cursor:pointer;}

#gbx_overlay{width:100%;height:100%;    position:fixed;    left:0;    top:0;    margin:0;    z-index:2999;
    display:none;    background:#000;     opacity:.50;    filter: alpha(opacity=50);     -moz-opacity: 0.5;}

* html #gbx_overlay { position:absolute; /*IE does not work with position fixed*/
    top:expression((0 + (ignoreMe = document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop)) + 'px'); 
    right: expression((0 + (ignoreMe2 = document.documentElement.scrollLeft ? document.documentElement.scrollLeft : document.body.scrollLeft)) + 'px');
    width:110%;
}

";


         @unlink('../../css/format.css');
         $tmm = @fopen('../../css/format.css', 'w+');
         @fwrite($tmm, $FORMATCSS);
         @fclose($tmm);


//DB

$rf=mysql_query(" UPDATE vob_format SET
body_bg='$body_bg',
text_clr='$text_clr',
text_font='$text_font',
font_size='$font_size',
container_bg='$container_bg',
container_borderradius='$container_borderradius',
wgtab_bg='$wgtab_bg',
wgtabhead_bg='$wgtabhead_bg',
wgtab_bg_o='$wgtab_bg_o',
wgtab_bg_u='$wgtab_bg_u',
wgtab_clr='$wgtab_clr',
contenbox_bg='$contenbox_bg',
instance_color='$instance_color'
WHERE template_set='format.css' ");






}else{

// Vorbelegung aus DB 

$rf=mysql_query(" SELECT * FROM vob_format WHERE template_set='format.css' ");
$res_rf=mysql_fetch_object($rf);
$body_bg=$res_rf->body_bg;
$text_clr=$res_rf->text_clr;
$text_font=$res_rf->text_font;
$font_size=$res_rf->font_size;
$container_bg=$res_rf->container_bg;
$container_borderradius=$res_rf->container_borderradius;
$wgtab_bg=$res_rf->wgtab_bg;
$wgtabhead_bg=$res_rf->wgtabhead_bg;
$wgtab_bg_o=$res_rf->wgtab_bg_o;
$wgtab_bg_u=$res_rf->wgtab_bg_u;
$wgtab_clr=$res_rf->wgtab_clr;
$contenbox_bg=$res_rf->contenbox_bg;
$instance_color=$res_rf->instance_color;



}



?>
 
 
 <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6/jquery.min.js"></script>

<script type="text/javascript">
$(document).ready(function(){
  
  
  
  $('.farbkachel').click(function() {
		var farbcode=$(this).attr('alt');
     //console.log(farbcode);
    $('#bgfarbepreview').css('background', farbcode);
    var string =  farbcode.substring(1,7);
    $('#bgfarbe').val(string);
	});
  
  
  $('.chgfield').blur(function() {
  var v = $(this).val();
  var r = $(this).attr('rel');
      if(v.length==6){
      $("#"+r).css("background", "#"+v);
      }
  });
  
});
  
</script>
 
 <style>
 input{height:22px;}
.demo{width:100px;height:24px;}
 </style>
 
<!-- einlesen
in var schreiben

datei format.css zum Download anbieten

Hinweis: bitte per FTP hochladen (Datei &uuml;berschreiben)
ggf. Temp. Daten l&ouml;schen (falls keine &Auml;nderungen sofort sichtbar sind)
 //-->


 
<h2>Darstellung von vionlink OBS &auml;ndern - format.css - BETA</h2>
<br>

<form method="post">
<table class="selection" cellspacing="2">


<tr>
<td><b>1.</b> Hintergrundfarbe der Webseite</td>
<td>#<input type="text" class="chgfield" rel="d1" name="body_bg" value="<?php echo $body_bg; ?>" /></td>
<td><div class="demo" id="d1" style="background:#<?php echo $body_bg; ?>;">&nbsp;</div></td>
</tr>

<tr>
<td><b>2.</b> Textfarbe (allgemein)</td>
<td>#<input type="text" class="chgfield" rel="d2" name="text_clr" value="<?php echo $text_clr; ?>" /></td>
<td><div class="demo" id="d2" style="background:#<?php echo $text_clr; ?>;">&nbsp;</div></td>
</tr>


<tr>
<td><b>3.</b> Schriftart (allgemein)</td><td>
&nbsp;&nbsp;<select name="text_font">
<option <?php echo($text_font=="verdana")?'selected':''; ?>>verdana</option>
<option <?php echo($text_font=="arial" || $text_font=="")?'selected':''; ?>>arial</option>
<option <?php echo($text_font=="courier")?'selected':''; ?>>courier</option>
<option <?php echo($text_font=="georgia")?'selected':''; ?>>georgia</option>
<option <?php echo($text_font=="times")?'selected':''; ?>>times</option>
</select></td>
</tr>

<tr>
<td><b>4.</b> Schriftgroesse (allgemein)</td><td>
&nbsp;&nbsp;<select name="font_size">
<option <?php echo($font_size=="10")?'selected':''; ?>>10</option>
<option <?php echo($font_size=="11")?'selected':''; ?>>11</option>
<option <?php echo($font_size=="12")?'selected':''; ?>>12</option>
<option <?php echo($font_size=="13")?'selected':''; ?>>13</option>
<option <?php echo($font_size=="14" || $font_size=="" )?'selected':''; ?>>14</option>
<option <?php echo($font_size=="15")?'selected':''; ?>>15</option>
<option <?php echo($font_size=="16")?'selected':''; ?>>16</option>
</select></td>
</tr>

<tr>
<td><b>5.</b> Hintergrundfarbe Hauptelement der Webseite</td>
<td>#<input type="text" class="chgfield" rel="d3" name="container_bg" value="<?php echo $container_bg; ?>" /></td>
<td><div class="demo" id="d3" style="background:#<?php echo $container_bg; ?>;">&nbsp;</div></td>
</tr>

<tr>
<td><b>6.</b> Rahmen Radius Hauptelement der Webseite</td>
<td>&nbsp;&nbsp;<input type="text" name="container_borderradius" value="<?php echo $container_borderradius; ?>" size="5" />px</td>
</tr>


<tr>
<td><b>7.</b> Hintergrundfarbe der Warengruppentabelle</td>
<td>#<input type="text" class="chgfield" rel="d4" name="wgtab_bg" value="<?php echo $wgtab_bg; ?>" /></td>
<td><div class="demo" id="d4" style="background:#<?php echo $wgtab_bg; ?>;">&nbsp;</div></td>
</tr>

<tr>
<td><b>8.</b> Hintergrundfarbe der Warengruppentabelle - Kopfzeile </td>
<td>#<input type="text" class="chgfield" rel="d5" name="wgtabhead_bg" value="<?php echo $wgtabhead_bg; ?>" /></td>
<td><div class="demo" id="d5" style="background:#<?php echo $wgtabhead_bg; ?>;">&nbsp;</div></td>
</tr>


<tr>
<td valign="top"><b>9.</b> Warengruppen Tabelle wechselnde Hintergrundfarben</td><td>
#<input type="text" class="chgfield" rel="d6" name="wgtab_bg_o" value="<?php echo $wgtab_bg_o; ?>" /><br>
#<input type="text" class="chgfield" rel="d7" name="wgtab_bg_u" value="<?php echo $wgtab_bg_u; ?>" />
</td><td>
<div class="demo" id="d6" style="background:#<?php echo $wgtab_bg_o; ?>;">&nbsp;</div><br>
<div class="demo" id="d7" style="background:#<?php echo $wgtab_bg_u; ?>;">&nbsp;</div>
</td>
</tr>


<tr>
<td><b>10.</b> Schriftfarbe Warengruppentabelle</td>
<td>#<input type="text" rel="d8" class="chgfield" name="wgtab_clr" value="<?php echo $wgtab_clr; ?>" /></td>
<td><div class="demo" id="d8" style="background:#<?php echo $wgtab_clr; ?>;">&nbsp;</div></td>
</tr>

<tr>
<td><b>11.</b> Hintergrundfarbe Inhaltsbereich</td>
<td>#<input type="text" class="chgfield" rel="d9" name="contenbox_bg" value="<?php echo $contenbox_bg; ?>" /></td>
<td><div class="demo" id="d9" style="background:#<?php echo $contenbox_bg; ?>;">&nbsp;</div></td>
</tr>

<tr>
<td><b>12.</b> Hintergrundfarbe Navigation, Bestellschein, Sideboxen</td>
<td>#<input type="text" class="chgfield" rel="d10" name="instance_color" value="<?php echo $instance_color; ?>" /></td>
<td><div class="demo" id="d10" style="background:#<?php echo $instance_color; ?>;">&nbsp;</div></td>
</tr>

 

<tr><td colspan="3">
<br>
<input type="submit" name="submit" value="absenden" /></td></tr>
<br>
</table>
</form>

 
 <br><br>
 <hr><br>
 <h2>Hilfe</h2>
 <br>

 <table><tr><td valign="top">
 <h3>Farbe</h3>
<img alt="colormap" usemap="#colormap" src="colormap.gif" style="margin-right:2px;">
<map  name="colormap" id="colormap">
<area alt="#003366" class="farbkachel" coords="63,0,72,4,72,15,63,19,54,15,54,4" shape="poly" style="cursor:pointer">
<area alt="#336699" class="farbkachel" coords="81,0,90,4,90,15,81,19,72,15,72,4" shape="poly" style="cursor:pointer">
<area alt="#3366CC" class="farbkachel" coords="99,0,108,4,108,15,99,19,90,15,90,4" shape="poly" style="cursor:pointer">
<area alt="#003399" class="farbkachel" coords="117,0,126,4,126,15,117,19,108,15,108,4" shape="poly" style="cursor:pointer">
<area alt="#000099" class="farbkachel" coords="135,0,144,4,144,15,135,19,126,15,126,4" shape="poly" style="cursor:pointer">
<area alt="#0000CC" class="farbkachel" coords="153,0,162,4,162,15,153,19,144,15,144,4" shape="poly" style="cursor:pointer">
<area alt="#000066" class="farbkachel" coords="171,0,180,4,180,15,171,19,162,15,162,4" shape="poly" style="cursor:pointer">
<area alt="#006666" class="farbkachel" coords="54,15,63,19,63,30,54,34,45,30,45,19" shape="poly" style="cursor:pointer">
<area alt="#006699" class="farbkachel" coords="72,15,81,19,81,30,72,34,63,30,63,19" shape="poly" style="cursor:pointer">
<area alt="#0099CC" class="farbkachel" coords="90,15,99,19,99,30,90,34,81,30,81,19" shape="poly" style="cursor:pointer">
<area alt="#0066CC" class="farbkachel" coords="108,15,117,19,117,30,108,34,99,30,99,19" shape="poly" style="cursor:pointer">
<area alt="#0033CC" class="farbkachel" coords="126,15,135,19,135,30,126,34,117,30,117,19" shape="poly" style="cursor:pointer">
<area alt="#0000FF" class="farbkachel" coords="144,15,153,19,153,30,144,34,135,30,135,19" shape="poly" style="cursor:pointer">
<area alt="#3333FF" class="farbkachel" coords="162,15,171,19,171,30,162,34,153,30,153,19" shape="poly" style="cursor:pointer">
<area alt="#333399" class="farbkachel" coords="180,15,189,19,189,30,180,34,171,30,171,19" shape="poly" style="cursor:pointer">
<area alt="#669999" class="farbkachel" coords="45,30,54,34,54,45,45,49,36,45,36,34" shape="poly" style="cursor:pointer">
<area alt="#009999" class="farbkachel" coords="63,30,72,34,72,45,63,49,54,45,54,34" shape="poly" style="cursor:pointer">
<area alt="#33CCCC" class="farbkachel" coords="81,30,90,34,90,45,81,49,72,45,72,34" shape="poly" style="cursor:pointer">
<area alt="#00CCFF" class="farbkachel" coords="99,30,108,34,108,45,99,49,90,45,90,34" shape="poly" style="cursor:pointer">
<area alt="#0099FF" class="farbkachel" coords="117,30,126,34,126,45,117,49,108,45,108,34" shape="poly" style="cursor:pointer">
<area alt="#0066FF" class="farbkachel" coords="135,30,144,34,144,45,135,49,126,45,126,34" shape="poly" style="cursor:pointer">
<area alt="#3366FF" class="farbkachel" coords="153,30,162,34,162,45,153,49,144,45,144,34" shape="poly" style="cursor:pointer">
<area alt="#3333CC" class="farbkachel" coords="171,30,180,34,180,45,171,49,162,45,162,34" shape="poly" style="cursor:pointer">
<area alt="#666699" class="farbkachel" coords="189,30,198,34,198,45,189,49,180,45,180,34" shape="poly" style="cursor:pointer">
<area alt="#339966" class="farbkachel" coords="36,45,45,49,45,60,36,64,27,60,27,49" shape="poly" style="cursor:pointer">
<area alt="#00CC99" class="farbkachel" coords="54,45,63,49,63,60,54,64,45,60,45,49" shape="poly" style="cursor:pointer">
<area alt="#00FFCC" class="farbkachel" coords="72,45,81,49,81,60,72,64,63,60,63,49" shape="poly" style="cursor:pointer">
<area alt="#00FFFF" class="farbkachel" coords="90,45,99,49,99,60,90,64,81,60,81,49" shape="poly" style="cursor:pointer">
<area alt="#33CCFF" class="farbkachel" coords="108,45,117,49,117,60,108,64,99,60,99,49" shape="poly" style="cursor:pointer">
<area alt="#3399FF" class="farbkachel" coords="126,45,135,49,135,60,126,64,117,60,117,49" shape="poly" style="cursor:pointer">
<area alt="#6699FF" class="farbkachel" coords="144,45,153,49,153,60,144,64,135,60,135,49" shape="poly" style="cursor:pointer">
<area alt="#6666FF" class="farbkachel" coords="162,45,171,49,171,60,162,64,153,60,153,49" shape="poly" style="cursor:pointer">
<area alt="#6600FF" class="farbkachel" coords="180,45,189,49,189,60,180,64,171,60,171,49" shape="poly" style="cursor:pointer">
<area alt="#6600CC" class="farbkachel" coords="198,45,207,49,207,60,198,64,189,60,189,49" shape="poly" style="cursor:pointer">
<area alt="#339933" class="farbkachel" coords="27,60,36,64,36,75,27,79,18,75,18,64" shape="poly" style="cursor:pointer">
<area alt="#00CC66" class="farbkachel" coords="45,60,54,64,54,75,45,79,36,75,36,64" shape="poly" style="cursor:pointer">
<area alt="#00FF99" class="farbkachel" coords="63,60,72,64,72,75,63,79,54,75,54,64" shape="poly" style="cursor:pointer">
<area alt="#66FFCC" class="farbkachel" coords="81,60,90,64,90,75,81,79,72,75,72,64" shape="poly" style="cursor:pointer">
<area alt="#66FFFF" class="farbkachel" coords="99,60,108,64,108,75,99,79,90,75,90,64" shape="poly" style="cursor:pointer">
<area alt="#66CCFF" class="farbkachel" coords="117,60,126,64,126,75,117,79,108,75,108,64" shape="poly" style="cursor:pointer">
<area alt="#99CCFF" class="farbkachel" coords="135,60,144,64,144,75,135,79,126,75,126,64" shape="poly" style="cursor:pointer">
<area alt="#9999FF" class="farbkachel" coords="153,60,162,64,162,75,153,79,144,75,144,64" shape="poly" style="cursor:pointer">
<area alt="#9966FF" class="farbkachel" coords="171,60,180,64,180,75,171,79,162,75,162,64" shape="poly" style="cursor:pointer">
<area alt="#9933FF" class="farbkachel" coords="189,60,198,64,198,75,189,79,180,75,180,64" shape="poly" style="cursor:pointer">
<area alt="#9900FF" class="farbkachel" coords="207,60,216,64,216,75,207,79,198,75,198,64" shape="poly" style="cursor:pointer">
<area alt="#006600" class="farbkachel" coords="18,75,27,79,27,90,18,94,9,90,9,79" shape="poly" style="cursor:pointer">
<area alt="#00CC00" class="farbkachel" coords="36,75,45,79,45,90,36,94,27,90,27,79" shape="poly" style="cursor:pointer">
<area alt="#00FF00" class="farbkachel" coords="54,75,63,79,63,90,54,94,45,90,45,79" shape="poly" style="cursor:pointer">
<area alt="#66FF99" class="farbkachel" coords="72,75,81,79,81,90,72,94,63,90,63,79" shape="poly" style="cursor:pointer">
<area alt="#99FFCC" class="farbkachel" coords="90,75,99,79,99,90,90,94,81,90,81,79" shape="poly" style="cursor:pointer">
<area alt="#CCFFFF" class="farbkachel" coords="108,75,117,79,117,90,108,94,99,90,99,79" shape="poly" style="cursor:pointer">
<area alt="#CCCCFF" class="farbkachel" coords="126,75,135,79,135,90,126,94,117,90,117,79" shape="poly" style="cursor:pointer">
<area alt="#CC99FF" class="farbkachel" coords="144,75,153,79,153,90,144,94,135,90,135,79" shape="poly" style="cursor:pointer">
<area alt="#CC66FF" class="farbkachel" coords="162,75,171,79,171,90,162,94,153,90,153,79" shape="poly" style="cursor:pointer">
<area alt="#CC33FF" class="farbkachel" coords="180,75,189,79,189,90,180,94,171,90,171,79" shape="poly" style="cursor:pointer">
<area alt="#CC00FF" class="farbkachel" coords="198,75,207,79,207,90,198,94,189,90,189,79" shape="poly" style="cursor:pointer">
<area alt="#9900CC" class="farbkachel" coords="216,75,225,79,225,90,216,94,207,90,207,79" shape="poly" style="cursor:pointer">
<area alt="#003300" class="farbkachel" coords="9,90,18,94,18,105,9,109,0,105,0,94" shape="poly" style="cursor:pointer">
<area alt="#009933" class="farbkachel" coords="27,90,36,94,36,105,27,109,18,105,18,94" shape="poly" style="cursor:pointer">
<area alt="#33CC33" class="farbkachel" coords="45,90,54,94,54,105,45,109,36,105,36,94" shape="poly" style="cursor:pointer">
<area alt="#66FF66" class="farbkachel" coords="63,90,72,94,72,105,63,109,54,105,54,94" shape="poly" style="cursor:pointer">
<area alt="#99FF99" class="farbkachel" coords="81,90,90,94,90,105,81,109,72,105,72,94" shape="poly" style="cursor:pointer">
<area alt="#CCFFCC" class="farbkachel" coords="99,90,108,94,108,105,99,109,90,105,90,94" shape="poly" style="cursor:pointer">
<area alt="#FFFFFF" class="farbkachel" coords="117,90,126,94,126,105,117,109,108,105,108,94" shape="poly" style="cursor:pointer">
<area alt="#FFCCFF" class="farbkachel" coords="135,90,144,94,144,105,135,109,126,105,126,94" shape="poly" style="cursor:pointer">
<area alt="#FF99FF" class="farbkachel" coords="153,90,162,94,162,105,153,109,144,105,144,94" shape="poly" style="cursor:pointer">
<area alt="#FF66FF" class="farbkachel" coords="171,90,180,94,180,105,171,109,162,105,162,94" shape="poly" style="cursor:pointer">
<area alt="#FF00FF" class="farbkachel" coords="189,90,198,94,198,105,189,109,180,105,180,94" shape="poly" style="cursor:pointer">
<area alt="#CC00CC" class="farbkachel" coords="207,90,216,94,216,105,207,109,198,105,198,94" shape="poly" style="cursor:pointer">
<area alt="#660066" class="farbkachel" coords="225,90,234,94,234,105,225,109,216,105,216,94" shape="poly" style="cursor:pointer">
<area alt="#336600" class="farbkachel" coords="18,105,27,109,27,120,18,124,9,120,9,109" shape="poly" style="cursor:pointer">
<area alt="#009900" class="farbkachel" coords="36,105,45,109,45,120,36,124,27,120,27,109" shape="poly" style="cursor:pointer">
<area alt="#66FF33" class="farbkachel" coords="54,105,63,109,63,120,54,124,45,120,45,109" shape="poly" style="cursor:pointer">
<area alt="#99FF66" class="farbkachel" coords="72,105,81,109,81,120,72,124,63,120,63,109" shape="poly" style="cursor:pointer">
<area alt="#CCFF99" class="farbkachel" coords="90,105,99,109,99,120,90,124,81,120,81,109" shape="poly" style="cursor:pointer">
<area alt="#FFFFCC" class="farbkachel" coords="108,105,117,109,117,120,108,124,99,120,99,109" shape="poly" style="cursor:pointer">
<area alt="#FFCCCC" class="farbkachel" coords="126,105,135,109,135,120,126,124,117,120,117,109" shape="poly" style="cursor:pointer">
<area alt="#FF99CC" class="farbkachel" coords="144,105,153,109,153,120,144,124,135,120,135,109" shape="poly" style="cursor:pointer">
<area alt="#FF66CC" class="farbkachel" coords="162,105,171,109,171,120,162,124,153,120,153,109" shape="poly" style="cursor:pointer">
<area alt="#FF33CC" class="farbkachel" coords="180,105,189,109,189,120,180,124,171,120,171,109" shape="poly" style="cursor:pointer">
<area alt="#CC0099" class="farbkachel" coords="198,105,207,109,207,120,198,124,189,120,189,109" shape="poly" style="cursor:pointer">
<area alt="#993399" class="farbkachel" coords="216,105,225,109,225,120,216,124,207,120,207,109" shape="poly" style="cursor:pointer">
<area alt="#333300" class="farbkachel" coords="27,120,36,124,36,135,27,139,18,135,18,124" shape="poly" style="cursor:pointer">
<area alt="#669900" class="farbkachel" coords="45,120,54,124,54,135,45,139,36,135,36,124" shape="poly" style="cursor:pointer">
<area alt="#99FF33" class="farbkachel" coords="63,120,72,124,72,135,63,139,54,135,54,124" shape="poly" style="cursor:pointer">
<area alt="#CCFF66" class="farbkachel" coords="81,120,90,124,90,135,81,139,72,135,72,124" shape="poly" style="cursor:pointer">
<area alt="#FFFF99" class="farbkachel" coords="99,120,108,124,108,135,99,139,90,135,90,124" shape="poly" style="cursor:pointer">
<area alt="#FFCC99" class="farbkachel" coords="117,120,126,124,126,135,117,139,108,135,108,124" shape="poly" style="cursor:pointer">
<area alt="#FF9999" class="farbkachel" coords="135,120,144,124,144,135,135,139,126,135,126,124" shape="poly" style="cursor:pointer">
<area alt="#FF6699" class="farbkachel" coords="153,120,162,124,162,135,153,139,144,135,144,124" shape="poly" style="cursor:pointer">
<area alt="#FF3399" class="farbkachel" coords="171,120,180,124,180,135,171,139,162,135,162,124" shape="poly" style="cursor:pointer">
<area alt="#CC3399" class="farbkachel" coords="189,120,198,124,198,135,189,139,180,135,180,124" shape="poly" style="cursor:pointer">
<area alt="#990099" class="farbkachel" coords="207,120,216,124,216,135,207,139,198,135,198,124" shape="poly" style="cursor:pointer">
<area alt="#666633" class="farbkachel" coords="36,135,45,139,45,150,36,154,27,150,27,139" shape="poly" style="cursor:pointer">
<area alt="#99CC00" class="farbkachel" coords="54,135,63,139,63,150,54,154,45,150,45,139" shape="poly" style="cursor:pointer">
<area alt="#CCFF33" class="farbkachel" coords="72,135,81,139,81,150,72,154,63,150,63,139" shape="poly" style="cursor:pointer">
<area alt="#FFFF66" class="farbkachel" coords="90,135,99,139,99,150,90,154,81,150,81,139" shape="poly" style="cursor:pointer">
<area alt="#FFCC66" class="farbkachel" coords="108,135,117,139,117,150,108,154,99,150,99,139" shape="poly" style="cursor:pointer">
<area alt="#FF9966" class="farbkachel" coords="126,135,135,139,135,150,126,154,117,150,117,139" shape="poly" style="cursor:pointer">
<area alt="#FF6666" class="farbkachel" coords="144,135,153,139,153,150,144,154,135,150,135,139" shape="poly" style="cursor:pointer">
<area alt="#FF0066" class="farbkachel" coords="162,135,171,139,171,150,162,154,153,150,153,139" shape="poly" style="cursor:pointer">
<area alt="#CC6699" class="farbkachel" coords="180,135,189,139,189,150,180,154,171,150,171,139" shape="poly" style="cursor:pointer">
<area alt="#993366" class="farbkachel" coords="198,135,207,139,207,150,198,154,189,150,189,139" shape="poly" style="cursor:pointer">
<area alt="#999966" class="farbkachel" coords="45,150,54,154,54,165,45,169,36,165,36,154" shape="poly" style="cursor:pointer">
<area alt="#CCCC00" class="farbkachel" coords="63,150,72,154,72,165,63,169,54,165,54,154" shape="poly" style="cursor:pointer">
<area alt="#FFFF00" class="farbkachel" coords="81,150,90,154,90,165,81,169,72,165,72,154" shape="poly" style="cursor:pointer">
<area alt="#FFCC00" class="farbkachel" coords="99,150,108,154,108,165,99,169,90,165,90,154" shape="poly" style="cursor:pointer">
<area alt="#FF9933" class="farbkachel" coords="117,150,126,154,126,165,117,169,108,165,108,154" shape="poly" style="cursor:pointer">
<area alt="#FF6600" class="farbkachel" coords="135,150,144,154,144,165,135,169,126,165,126,154" shape="poly" style="cursor:pointer">
<area alt="#FF5050" class="farbkachel" coords="153,150,162,154,162,165,153,169,144,165,144,154" shape="poly" style="cursor:pointer">
<area alt="#CC0066" class="farbkachel" coords="171,150,180,154,180,165,171,169,162,165,162,154" shape="poly" style="cursor:pointer">
<area alt="#660033" class="farbkachel" coords="189,150,198,154,198,165,189,169,180,165,180,154" shape="poly" style="cursor:pointer">
<area alt="#996633" class="farbkachel" coords="54,165,63,169,63,180,54,184,45,180,45,169" shape="poly" style="cursor:pointer">
<area alt="#CC9900" class="farbkachel" coords="72,165,81,169,81,180,72,184,63,180,63,169" shape="poly" style="cursor:pointer">
<area alt="#FF9900" class="farbkachel" coords="90,165,99,169,99,180,90,184,81,180,81,169" shape="poly" style="cursor:pointer">
<area alt="#CC6600" class="farbkachel" coords="108,165,117,169,117,180,108,184,99,180,99,169" shape="poly" style="cursor:pointer">
<area alt="#FF3300" class="farbkachel" coords="126,165,135,169,135,180,126,184,117,180,117,169" shape="poly" style="cursor:pointer">
<area alt="#FF0000" class="farbkachel" coords="144,165,153,169,153,180,144,184,135,180,135,169" shape="poly" style="cursor:pointer">
<area alt="#CC0000" class="farbkachel" coords="162,165,171,169,171,180,162,184,153,180,153,169" shape="poly" style="cursor:pointer">
<area alt="#990033" class="farbkachel" coords="180,165,189,169,189,180,180,184,171,180,171,169" shape="poly" style="cursor:pointer">
<area alt="#663300" class="farbkachel" coords="63,180,72,184,72,195,63,199,54,195,54,184" shape="poly" style="cursor:pointer">
<area alt="#996600" class="farbkachel" coords="81,180,90,184,90,195,81,199,72,195,72,184" shape="poly" style="cursor:pointer">
<area alt="#CC3300" class="farbkachel" coords="99,180,108,184,108,195,99,199,90,195,90,184" shape="poly" style="cursor:pointer">
<area alt="#993300" class="farbkachel" coords="117,180,126,184,126,195,117,199,108,195,108,184" shape="poly" style="cursor:pointer">
<area alt="#990000" class="farbkachel" coords="135,180,144,184,144,195,135,199,126,195,126,184" shape="poly" style="cursor:pointer">
<area alt="#800000" class="farbkachel" coords="153,180,162,184,162,195,153,199,144,195,144,184" shape="poly" style="cursor:pointer">
<area alt="#993333" class="farbkachel" coords="171,180,180,184,180,195,171,199,162,195,162,184" shape="poly" style="cursor:pointer">
</map>

<br><br>
gew&auml;hlte Farbe:
<div style="width:100px;height: 30px;" id="bgfarbepreview">&nbsp;</div>
<br>Hexcode: #<input id="bgfarbe" name="bgfarbe" style="width:70px;"   value="" >
<br><br>


</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td>
<img src="obs-formatieren.jpg"  border="0" alt="" />
</td></tr></table>

<br>
<br>
<br>
<br>