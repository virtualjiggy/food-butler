<?php 
##############################################################################
##############################################################################

    error_reporting(0);

    @session_start(); 
    if($_SESSION['admin']!="ontime"){
    header("Location: ../login.php");
    exit();
    }
    
##############################################################################
##############################################################################

    define('IMSCRIPT', '1');
    @require("../inclx/db_vbdg.php");
    @require("../inclx/config.php");
    @require("../inclx/funcx.php");
    @require("../inclx/meldungen.php");
    header("Content-Type: text/html; charset=$SCHRIFTSATZ");
    
##############################################################################
##############################################################################
?>


<!DOCTYPE HTML>
<html>
<head>

    <title>Administration - Zaza Kebap</title>
    <meta http-equiv="content-type" content="text/html; charset=<?php echo $SCHRIFTSATZ; ?>">
    <link rel="stylesheet" type="text/css" href="mas.layout.css">



<link rel="stylesheet" type="text/css" href="../styling.css" />  
<script type="text/javascript" src="../js/jquery-1.3.2.min.js"></script>

<script language="JavaScript" type="text/JavaScript">
function queryWG(id){
if (id=="none")return;
location.href="details.php?warengruppe="+id+"";
}
function queryArt(id){
if (id=="none")return;
location.href="details.php?id="+id+"";
}
function queryRub(id){
if (id=="none")return;
location.href="details.php?id="+id+"";
}
function queryZut(id){
if (id=="none")return;
location.href="display.php?ez="+id+"";
}
</script>

    <?php if(file_exists("../js/jquery-ui.min.js")){ ?>
        <link rel="stylesheet" href="../js/jquery-ui.css" type="text/css">
        <script type="text/javascript" src="../js/jquery-ui.min.js"></script>
        <script type="text/javascript">
            $(function(){$('.dateselector').datepicker({ dateFormat: 'dd.mm.yy' });});
        </script>
        <style type="text/css">
            #ui-datepicker-div{width:300px;}
            .ui-datepicker .ui-datepicker-prev span {border:1px solid #fff;background:url(../js/arrow_left.jpg) no-repeat;}
            .ui-datepicker .ui-datepicker-next span {border:1px solid #fff;background:url(../js/arrow_right.jpg) no-repeat;}
            .selection{width:600px;}
        </style>
    <?php } ?>


</head>
<body>

<div id="l"><?php @include("../navi.php"); ?></div>
<div id="r">