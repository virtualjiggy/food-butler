<?php  
##############################################################################
##############################################################################
###________________________________________________________________________###
###                                                                        ###
###             vionlink obs 3.4 by vision impress webdesign               ###
###             written 2009/15  by vision impress webdesign               ###
###________________________________________________________________________###
###                                                                        ###
##############################################################################
##############################################################################
         

    error_reporting(0);
    session_start();      
         
##############################################################################
##############################################################################
                    
    define('IMSCRIPT', '1');
    @include("administration/inclx/db_vbdg.php");
    @include("administration/inclx/config.php");
    @include("administration/inclx/funcx.php");


##############################################################################
##############################################################################


    @include("inclx/showlist.funcx.php");
    @include("inclx/showlist.inc.php");
    @include("inclx/openorclosed.php");
    @include("inclx/detectmobilebrowser.php");

    @include("inclw/oeffnungszeiten.php");
    @include("inclw/zusatzstoffe.php");
    @include("inclw/produktnavigation.php");


##############################################################################
##############################################################################

    $zliste='';
    $js_4_mess='';
    $open='';
    $summe ='';
    $bnlid='';
    $pp_name='';


##############################################################################
##############################################################################


if(isset($_POST['submit'])){

@include("inclx/bestellung_neukunde.inc.php");

}else{
    $newsletter_abo='';
    $anrede='';
    $vorname='';
    $name='';
    $vorwahl='';
    $rufnummer='';
    $mobilnummer='';
    $strasse='';
    $nummer='';
    $liefergebiet='';
    $maincontent='';
    $email='';
    $firma='';
    $abteilung='';
    $besonderheiten='';
    $fehler_tt='';
}

##############################################################################
##############################################################################



if($_SESSION['bestellungok']==0){
    $fehler=1;
    $err_mess='der Mindestbestellwert ist nicht erreicht!';
}


if($selbstabholung == 'on' ){
    //Anfahrt auf 0 setzen

    $anfahrt=0;

}


##############################################################################
##############################################################################


// Template Ordner

switch($mobile){
    case "0": $template_ordner='templates'; break;
    case "1": $template_ordner='templates_mobile'; break;
}

##############################################################################
##############################################################################


if(isset($fehler)){ 
$fehler_tt='<tr><td align="center"><br><br><div class="fehlermeldung">'.$err_mess.'</div></td></tr>';
}  


     if($closed==1){   // geschlossen, vorbestellung mgl.


        $OPEN_DISPLAY='';
        $form_closed=1;

    }else{
        // kein Vorbestellungskasten
        $form_closed=0;
        $OPEN_DISPLAY=' style="display:none;" ';

    }



// closed 0  IST  open = 1
// closed 1 = Vorbestellung mgl.
// closed 2 = Vorbestellung ausgeschaltet
// closed 3 = Ruhetag (keine Vorbestellung)
// closed 4 = Wartung $WARTUNG



if($show_newsletter==0){$NEWSLETTER_DISPLAY=' style="display:none;" ';}else{$NEWSLETTER_DISPLAY='';}
if($show_gutschein==0) {$GUTSCHEIN_DISPLAY=' style="display:none;" ';}else{$GUTSCHEIN_DISPLAY='';}
if($KUNDENSYSTEM!=1)   {$LOGIN_DISPLAY=' style="display:none;" ';}else{$LOGIN_DISPLAY='';}
if($newsletter_abo==1){$nlabo_checked='checked';}else{$nlabo_checked='';}


    // wenn eine Z-Methode angeschaltet
    if(
    $PAYMENT['bar'][0]          == 1 ||
    $PAYMENT['ec'][0]           == 1 ||
    $PAYMENT['paypal'][0]       == 1 ||
    $PAYMENT['sofort'][0]       == 1 ||
    $PAYMENT['masterpayment'][0]== 1
    ){

    $BEZ ='
    <tr><td colspan="2">&nbsp;</td></tr>
    <tr><td align="right"><b>wie m&ouml;chten Sie zahlen?</b></td><td>';
    

    $c1 = ($bezahlung=='1')? 'checked' : ''; // Bar
    $c2 = ($bezahlung=='2')? 'checked' : ''; // EC
    $c3 = ($bezahlung=='3')? 'checked' : ''; // PayPal
    $c4 = ($bezahlung=='4')? 'checked' : ''; // SOFORT
    $c5 = ($bezahlung=='5')? 'checked' : ''; // Masterpayment


    if( $PAYMENT['bar'][0]      ==1){$BEZ .='<input type="radio" name="bezahlung" value="1" '.$c1.'>'.$PAYMENT['bar'][1].'<br>';}
    if( $PAYMENT['ec'][0]       ==1){$BEZ .='<input type="radio" name="bezahlung" value="2" '.$c2.'>'.$PAYMENT['ec'][1].'<br>';}
    if( $PAYMENT['paypal'][0]   ==1){$BEZ .='<input type="radio" name="bezahlung" value="3" '.$c3.'>'.$PAYMENT['paypal'][1].'<br>';}
    if( $PAYMENT['sofort'][0]   ==1){$BEZ .='<input type="radio" name="bezahlung" value="4" '.$c4.'>'.$PAYMENT['sofort'][1].'<br>';}
    if( $PAYMENT['masterpayment'][0]   ==1){$BEZ .='<input type="radio" name="bezahlung" value="5" '.$c5.'>'.$PAYMENT['masterpayment'][1].'<br>';}


    $BEZ .='</td></tr><tr><td colspan="2">&nbsp;</td></tr>';

}else{

    $BEZ ='<input type="hidden" name="bezahlung" value="bar">';
    
}

##########################################################################################
##########################################################################################



 if($SELBSTABHOLER==1){

     $d1 = ($selbstabholung == 'on')? 'checked' : '';

     $ckbox_selbstabholer ='    <tr><td colspan="2">&nbsp;</td></tr>
     <tr><td align="right"><b>m&ouml;chten Sie selbstabholen?</b></td><td>';

     $ckbox_selbstabholer .='<input type="checkbox" name="selbstabholung"  '.$d1.'> ja, ich werde die Bestellung selbst abholen <br>';

     $ckbox_selbstabholer .='</td></tr><tr><td colspan="2">&nbsp;</td></tr>';

 } else{

     $ckbox_selbstabholer ='';

 }



##########################################################################################
##########################################################################################


    // Top Navigation
    $tn_tpl = @file_get_contents("$template_ordner/scme/top_navigation.html");
    $top_navi = preg_replace("=\[PATH\]=", $PATH, $tn_tpl);

    if(isset($_SESSION['kunden_ID'])){
    $top_navi = preg_replace("=\[KUNDENSEITE\]=", 'kundenmenu.php', $top_navi);
    $top_navi = preg_replace("=\[KUNDENSYSTEM\]=", 'Kundenmenu', $top_navi);
    }else{
    $top_navi = preg_replace("=\[KUNDENSEITE\]=", 'kundenlogin.php', $top_navi);
    $top_navi = preg_replace("=\[KUNDENSYSTEM\]=", 'Kunden Login', $top_navi);
    }

    // Footer - Template
    $footer_tpl = @file_get_contents("$template_ordner/scme/footer.html");
    $footer = preg_replace("=\[PATH\]=", $PATH, $footer_tpl);
    $footer = preg_replace("=\[COPYRIGHT\]=", $COPY, $footer);


##########################################################################################
##########################################################################################


    $template = '';
    $inhalt   = '';



    $inhalt = @join('', file("$template_ordner/bestellen_neukunde.html"));

    if($anrede=="Herr"){$anrede_herr_checked='checked';}else{$anrede_herr_checked='';}
    if($anrede=="Frau"){$anrede_frau_checked='checked';}else{$anrede_frau_checked='';}

    $inhalt = preg_replace("=\[ANREDE_HERR_CH\]=", $anrede_herr_checked, $inhalt);
    $inhalt = preg_replace("=\[ANREDE_FRAU_CH\]=", $anrede_frau_checked, $inhalt);

    $inhalt = preg_replace("=\[FORM_VORNAME\]=", $vorname, $inhalt);
    $inhalt = preg_replace("=\[FORM_NAME\]=", $name, $inhalt);


    $inhalt = preg_replace("=\[FORM_VORWAHL\]=", $vorwahl, $inhalt);
    $inhalt = preg_replace("=\[FORM_RUFNUMMER\]=", $rufnummer, $inhalt);
$inhalt = preg_replace("=\[FORM_MOBILNUMMER\]=", $mobilnummer, $inhalt);
    $inhalt = preg_replace("=\[FORM_STRASSE\]=", $strasse, $inhalt);
    $inhalt = preg_replace("=\[FORM_NUMMER\]=", $nummer, $inhalt);
    $inhalt = preg_replace("=\[FORM_PLZ\]=", $_SESSION['liefergebiet']['plz'], $inhalt);
    $inhalt = preg_replace("=\[FORM_ORT\]=", $_SESSION['liefergebiet']['ort'], $inhalt);

    if($mobile==0){
        $inhalt = preg_replace("=\[LOGO_IMG_SRC\]=", 'img/'.$LOGO, $inhalt);
    }else{
        $inhalt = preg_replace("=\[LOGO_IMG_SRC\]=", 'img/'.$MOBILLOGO, $inhalt);
    }
    $inhalt = preg_replace("=\[HEAD_LOGO_IMG_SRC\]=", 'img/'.$HEADLOGO, $inhalt);  

        $inhalt = preg_replace("=\[BACK_LOGO_IMG_SRC\]=", 'img/'.$BACKLOGO, $inhalt);

    $inhalt = preg_replace("=\[MAINCONTENT\]=", $maincontent, $inhalt);
    $inhalt = preg_replace("=\[FOOTER\]=", $footer, $inhalt);
    $inhalt = preg_replace("=\[NAVIGATION\]=", $create_navi, $inhalt);
    $inhalt = preg_replace("=\[TOPNAVIGATION\]=", $top_navi, $inhalt);
    $inhalt = preg_replace("=\[BESTELLSCHEIN\]=", $bestellschein, $inhalt);
    $inhalt = preg_replace("=\[OEFFNUNGSZEITEN\]=", $oeffzeiten, $inhalt);


    $inhalt = preg_replace("=\[FORM_EMAIL\]=", $email, $inhalt);
    $inhalt = preg_replace("=\[FORM_FIRMA\]=", $firma, $inhalt);
    $inhalt = preg_replace("=\[FORM_ABTEILUNG\]=", $abteilung, $inhalt);
    $inhalt = preg_replace("=\[FORM_BESONDERHEITEN\]=", $besonderheiten, $inhalt);

    $inhalt = preg_replace("=\[FORM_GUTSCHEIN\]=", $eingegebener_gutscheincode, $inhalt);
    $inhalt = preg_replace("=\[FORM_NEWSLETTER\]=", $nlabo_checked, $inhalt);
    $inhalt = preg_replace("=\[FORM_OPEN\]=", $form_closed, $inhalt);
    $inhalt = preg_replace("=\[FEHLER\]=", $fehler_tt, $inhalt);
    $inhalt = preg_replace("=\[SCHRIFTSATZ\]=", $SCHRIFTSATZ, $inhalt);
    $inhalt = preg_replace("=\[BEZAHLUNG\]=", $BEZ, $inhalt);
    $inhalt = preg_replace("=\[SELBSTABHOLUNG\]=", $ckbox_selbstabholer, $inhalt);

    $inhalt = preg_replace("=\[OPEN_DISPLAY\]=", $OPEN_DISPLAY, $inhalt);
    $inhalt = preg_replace("=\[NEWSLETTER_DISPLAY\]=", $NEWSLETTER_DISPLAY, $inhalt);
    $inhalt = preg_replace("=\[GUTSCHEIN_DISPLAY\]=", $GUTSCHEIN_DISPLAY, $inhalt);
    $inhalt = preg_replace("=\[LOGIN_DISPLAY\]=", $LOGIN_DISPLAY, $inhalt);



    header("Content-Type: text/html; charset=$SCHRIFTSATZ");
    echo $inhalt;
    exit();

##########################################################################################
##########################################################################################
?>