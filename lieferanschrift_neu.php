<?php  
##############################################################################
##############################################################################
###________________________________________________________________________###
###                                                                        ###
###             vionlink obs 3.4 by vision impress webdesign               ###
###             written 2009/15  by vision impress webdesign               ###
###________________________________________________________________________###
###                                                                        ###
##############################################################################
##############################################################################
                                                                              
##############################################################################
##############################################################################

    error_reporting(0);
    session_start();

    define('IMSCRIPT', '1');
    @include("administration/inclx/db_vbdg.php");
    @include("administration/inclx/funcx.php");
    @include("administration/inclx/config.php");


##############################################################################
##############################################################################


    @include("inclx/showlist.funcx.php");
    @include("inclx/showlist.inc.php");
    @include("inclx/openorclosed.php");
    @include("inclx/detectmobilebrowser.php");

    @include("inclw/oeffnungszeiten.php");
    @include("inclw/zusatzstoffe.php");
    @include("inclw/produktnavigation.php");


##############################################################################
##############################################################################

    // wenn Kunde nicht eingeloggt, weiterleiten auf Menu
    if(!isset($_SESSION['kunden_ID']) || $_SESSION['kunden_ID']==""){
    header("Location: kundenlogin.php");
    exit();
    }

##############################################################################
##############################################################################

    $err_mess='';
    $kundeID=$_SESSION['kunden_ID'];

##############################################################################
##############################################################################

    $e[5]='Bitte geben Sie Ihre Vorwahl ein';
    $e[6]='Bitte geben Sie Ihre Rufnummer ein';
    $e[7]='Bitte geben Sie Ihre Strasse ein';
    $e[8]='Bitte geben Sie Ihre Hausnummer ein';

    $e[11]='Die Vorwahl ist zu kurz';
    $e[12]='Die Rufnummer ist zu kurz';
    $e[13]='Die Vorwahl ist zu lang';
    $e[14]='Die Rufnummer ist zu lang';

##############################################################################
##############################################################################

if(isset($_POST['submit'])){

 
    $neues_lg = $_POST['neues_lg'];
    $strasse = $_POST['strasse'];
    $nummer = $_POST['nummer'];
    $vorwahl = $_POST['vorwahl'];
    $rufnummer = $_POST['rufnummer'];
    $mobilnummer = $_POST['mobilnummer'];
    $firma = $_POST['firma'];
    $abteilung = $_POST['abteilung'];
    $besonderheiten = $_POST['besonderheiten'];

    if($vorwahl==""){$fehler=1; $err_mess.=$e[5].'<br>';}
    if($rufnummer==""){$fehler=1; $err_mess.=$e[6].'<br>';}
    if($strasse==""){$fehler=1; $err_mess.=$e[7].'<br>';}
    if($nummer==""){$fehler=1; $err_mess.=$e[8].'<br>';}


    if(!isset($fehler)){
    if(strlen($vorwahl)<3){$fehler=1; $err_mess.=$e[11].'<br>';}
    if(strlen($rufnummer)<4){$fehler=1; $err_mess.=$e[12].'<br>';}
    if(strlen($vorwahl)>12){$fehler=1; $err_mess.=$e[13].'<br>';}
    if(strlen($rufnummer)>12){$fehler=1; $err_mess.=$e[14].'<br>';}
    }




    if(!isset($fehler)){
    
    // PLZ / Ort aus LG ID ermitteln
    $qww=mysql_query(" SELECT plz,ort FROM vob_liefergebiet WHERE id='$neues_lg' ");
    $rqw=@mysql_fetch_object($qww);
    $ort=$rqw->ort;    
    $plz=$rqw->plz;
    
    $r=@mysql_query(" SELECT id FROM vob_lieferanschriften ORDER by id desc LIMIT 1 ");
    $rq=@mysql_fetch_object($r);
    $lnid=$rq->id;
    $lnid++;
    
    @mysql_query(" INSERT INTO vob_lieferanschriften SET
    id='$lnid',
    to_kundenid='$kundeID', 	
    vorwahl='$vorwahl', 	
    rufnummer='$rufnummer',
    mobilnummer='$mobilnummer',
    strasse='$strasse', 
    nummer='$nummer', 
    plz='$plz', 
    ort='$ort', 	
    firma='$firma',
    abteilung='$abteilung',
    besonderheit='$besonderheiten',
    liefergebiet_id='$neues_lg'
    ");
    
    header("Location: kundenmenu.php");
    exit();
    
    }


}

##############################################################################
##############################################################################




$LIEFERGEBIETE ='<select name="neues_lg" style="min-width:170px;">';

$qw=mysql_query(" SELECT * FROM vob_liefergebiet ORDER by ort asc ");
while($res=mysql_fetch_object($qw)){

$LIEFERGEBIETE .='<option value="'.$res->id.'">'.$res->plz.' - '.$res->ort.'</option>';

}

$LIEFERGEBIETE .='</select>';


##############################################################################
##############################################################################

    // Template Ordner

    switch($mobile){
        case "0": $template_ordner='templates'; break;
        case "1": $template_ordner='templates_mobile'; break;
    }

##############################################################################
##############################################################################




    // Top Navigation
    $tn_tpl = @file_get_contents("$template_ordner/scme/top_navigation.html");
    $top_navi = preg_replace("=\[PATH\]=", $PATH, $tn_tpl);
    $top_navi = preg_replace("=\[KUNDENSEITE\]=", 'kundenmenu.php', $top_navi);
    $top_navi = preg_replace("=\[KUNDENSYSTEM\]=", 'Kundenmenu', $top_navi);


    // Footer - Template
    $footer_tpl = @file_get_contents("$template_ordner/scme/footer.html");
    $footer = preg_replace("=\[COPYRIGHT\]=", $COPY, $footer_tpl);
    $footer = preg_replace("=\[PATH\]=", $PATH, $footer);

    if(isset($fehler)){ $fehler_tt='<tr><td align="center"><br><br><div class="fehlermeldung">'.$err_mess.'</div></td></tr>';}


    $template = @join('', file("$template_ordner/lieferanschrift.html"));
    $inhalt = preg_replace("=\[MAINCONTENT\]=", '', $template);

    $inhalt = preg_replace("=\[FEHLER\]=", $fehler_tt, $inhalt);
    $inhalt = preg_replace("=\[LIEFERGEBIETE\]=", $LIEFERGEBIETE, $inhalt);

    $inhalt = preg_replace("=\[FORM_VORWAHL\]=", $vorwahl, $inhalt);
    $inhalt = preg_replace("=\[FORM_RUFNUMMER\]=", $rufnummer, $inhalt);
    $inhalt = preg_replace("=\[FORM_MOBILNUMMER\]=", $mobilnummer, $inhalt);
    $inhalt = preg_replace("=\[FORM_STRASSE\]=", $strasse, $inhalt);
    $inhalt = preg_replace("=\[FORM_NUMMER\]=", $nummer, $inhalt);
    $inhalt = preg_replace("=\[FORM_ABTEILUNG\]=", $abteilung, $inhalt);
    $inhalt = preg_replace("=\[FORM_BESONDERHEITEN\]=", $besonderheiten, $inhalt);
    $inhalt = preg_replace("=\[FORM_FIRMA\]=", $firma, $inhalt);

    $inhalt = preg_replace("=\[MELDUNGEN\]=", $fehlermeldung, $inhalt);
    $inhalt = preg_replace("=\[FOOTER\]=", $footer, $inhalt);

    if($mobile==0){
        $inhalt = preg_replace("=\[LOGO_IMG_SRC\]=", 'img/'.$LOGO, $inhalt);
    }else{
        $inhalt = preg_replace("=\[LOGO_IMG_SRC\]=", 'img/'.$MOBILLOGO, $inhalt);
    }
        $inhalt = preg_replace("=\[HEAD_LOGO_IMG_SRC\]=", 'img/'.$HEADLOGO, $inhalt);  
        $inhalt = preg_replace("=\[BACK_LOGO_IMG_SRC\]=", 'img/'.$BACKLOGO, $inhalt);
        
    $inhalt = preg_replace("=\[OEFFNUNGSZEITEN\]=", $oeffzeiten, $inhalt);
    $inhalt = preg_replace("=\[TOPNAVIGATION\]=", $top_navi, $inhalt);
    $inhalt = preg_replace("=\[NAVIGATION\]=", $create_navi, $inhalt);
    $inhalt = preg_replace("=\[BESTELLSCHEIN\]=", $bestellschein, $inhalt);
    $inhalt = preg_replace("=\[SCHRIFTSATZ\]=", $SCHRIFTSATZ, $inhalt);

    header("Content-Type: text/html; charset=$SCHRIFTSATZ");
    echo $inhalt;
    exit();


##############################################################################
##############################################################################
?>