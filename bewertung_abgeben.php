<?php  
##############################################################################
##############################################################################
###________________________________________________________________________###
###                                                                        ###
###             vionlink obs 3.4 by vision impress webdesign               ###
###             written 2009/15  by vision impress webdesign               ###
###________________________________________________________________________###
###                                                                        ###
##############################################################################
##############################################################################


##############################################################################
##############################################################################

    error_reporting(0);
    session_start();

    define('IMSCRIPT', '1');
    @include("administration/inclx/db_vbdg.php");
    @include("administration/inclx/funcx.php");
    @include("administration/inclx/config.php");

##############################################################################
##############################################################################


    @include("inclx/showlist.funcx.php");
    @include("inclx/showlist.inc.php");
    @include("inclx/openorclosed.php");
    @include("inclx/detectmobilebrowser.php");

    @include("inclw/oeffnungszeiten.php");
    @include("inclw/zusatzstoffe.php");
    @include("inclw/produktnavigation.php");


##############################################################################
##############################################################################


    if(isset($_POST['submit'])){@include("inclx/bewertung_absenden.inc.php");}


    if(isset($danke)){$dankemeldung= '<tr><td align="center"><br><br><div style="padding:10px;width:400px;text-align:left;">'.$danke.'</div></td></tr>';}
    if(isset($fehler)){$fehlermeldung='<tr><td align="center"><br><br><div style="border:2px solid red;color:red;padding:10px;width:400px;text-align:left;">'.$err_mess.'</div></td></tr>';}



##############################################################################
##############################################################################


    switch($mobile){
        case "0": $template_ordner='templates'; break;
        case "1": $template_ordner='templates_mobile'; break;
    }

##############################################################################
##############################################################################


    // Top Navigation
    $tn_tpl = @file_get_contents("$template_ordner/scme/top_navigation.html");
    $top_navi = preg_replace("=\[PATH\]=", $PATH, $tn_tpl);

    if(isset($_SESSION['kunden_ID'])){
    $top_navi = preg_replace("=\[KUNDENSEITE\]=", 'kundenmenu.php', $top_navi);
    $top_navi = preg_replace("=\[KUNDENSYSTEM\]=", 'Kundenmenu', $top_navi);
    }else{
    $top_navi = preg_replace("=\[KUNDENSEITE\]=", 'kundenlogin.php', $top_navi);
    $top_navi = preg_replace("=\[KUNDENSYSTEM\]=", 'Kunden Login', $top_navi);
    }


    // Footer - Template
    $footer_tpl = @file_get_contents("$template_ordner/scme/footer.html");
    $footer = preg_replace("=\[COPYRIGHT\]=", $COPY, $footer_tpl);
    $footer = preg_replace("=\[PATH\]=", $PATH, $footer);



    $template = @join('', file("$template_ordner/bewertung_abgeben.html"));
    $inhalt = preg_replace("=\[MAINCONTENT\]=", $maincontent, $template);
    $inhalt = preg_replace("=\[TOPNAVIGATION\]=",$top_navi, $inhalt);
    $inhalt = preg_replace("=\[OEFFNUNGSZEITEN\]=", $oeffzeiten, $inhalt);
    $inhalt = preg_replace("=\[FOOTER\]=", $Sinhalt, $inhalt);

    if($mobile==0){
        $inhalt = preg_replace("=\[LOGO_IMG_SRC\]=", 'img/'.$LOGO, $inhalt);
    }else{
        $inhalt = preg_replace("=\[LOGO_IMG_SRC\]=", 'img/'.$MOBILLOGO, $inhalt);
    }
$inhalt = preg_replace("=\[HEAD_LOGO_IMG_SRC\]=", 'img/'.$HEADLOGO, $inhalt);  

        $inhalt = preg_replace("=\[BACK_LOGO_IMG_SRC\]=", 'img/'.$BACKLOGO, $inhalt);
        
    $inhalt = preg_replace("=\[NAVIGATION\]=", $create_navi, $inhalt);
    $inhalt = preg_replace("=\[BESTELLSCHEIN\]=", $bestellschein, $inhalt);
    $inhalt = preg_replace("=\[DANKE\]=", $dankemeldung, $inhalt);
    $inhalt = preg_replace("=\[FEHLER\]=", $fehlermeldung, $inhalt);
    $inhalt = preg_replace("=\[FORM_SYS\]=", $formundis, $inhalt);
    $inhalt = preg_replace("=\[KOMMENTAR\]=", $post_kommentar, $inhalt);
    $inhalt = preg_replace("=\[SCHRIFTSATZ\]=", $SCHRIFTSATZ, $inhalt);


    header("Content-Type: text/html; charset=$SCHRIFTSATZ");
    echo $inhalt;
    exit();


##########################################################################################
##########################################################################################
?>